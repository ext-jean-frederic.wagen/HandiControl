# HandiControl Dataview

HandiControl Dataview permet de visualiser et de d'éditer les labels et les données avant leur utilisation par un système de machine learning.

## Dev

```sh
npm i
npm run dev
npm run start-dev
```

On Unix system, replace `npm run start-dev` with `npm run unix-start-dev`.

## Build prod

For Windows (on Windows or with *Wine*)
```sh
npm run win-dist
```

For Linux (works with *Linux subsystem for Windows*)
```sh
npm run linux-dist
```

For MacOs (on MacOs only)
```sh
npm run mac-dist
```