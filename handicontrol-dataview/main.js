// Basic init
const electron = require('electron');
const { app, BrowserWindow, Menu } = electron;

const path = require('path');
const url = require('url');

function isDev() {
    return process.env.ELECTRON_ENV === 'development';
}

// ffprobe preparation (to compute video duration)
let ffprobeStaticPath = '';
if (isDev())
    ffprobeStaticPath = require('ffprobe-static').path;
else {
    let ext = '';
    if (process.platform === 'win32') ext = '.exe'; // if windows
    ffprobeStaticPath = path.join(process.resourcesPath + '/ffprobe' + ext);
}
global.ffprobe = require('ffprobe');
global.ffprobeStaticPath = ffprobeStaticPath;


// Let electron reloads by itself when webpack watches changes in ./app/
if (process.env.ELECTRON_START_URL) {
    // require('electron-reload')(__dirname)
}


// To avoid being garbage collected
let mainWindow;

function createWindow() {
    let mainWindow = new BrowserWindow({
        width: 1600, height: 900, webPreferences: {
            webSecurity: false,
            nodeIntegration: true,
        },
        icon: path.join(__dirname, 'assets/favicon.png'),
    });

    const startUrl = process.env.ELECTRON_START_URL || url.format({
        pathname: path.join(__dirname, './build/index.html'),
        protocol: 'file:',
        slashes: true
    });

    mainWindow.loadURL(startUrl);

    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    });

    if (isDev()) {
        console.log('Dev mode, installing extensions...');
        const { default: installExtension, REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS, REACT_PERF } = require('electron-devtools-installer');
        installExtension(REACT_DEVELOPER_TOOLS).then((name) => console.log(`Added Extension:  ${name}`)).catch((err) => console.log('An error occurred: ', err));
        installExtension(REDUX_DEVTOOLS).then((name) => console.log(`Added Extension:  ${name}`)).catch((err) => console.log('An error occurred: ', err));
        installExtension(REACT_PERF).then((name) => console.log(`Added Extension:  ${name}`)).catch((err) => console.log('An error occurred: ', err));
    }
}

app.on('ready', createWindow);

Menu.setApplicationMenu(require('./src/main-process/menus').menu);

// Quit when all windows are closed.
app.on('window-all-closed', app.quit);

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    }
});
