var fs = require('fs');
var path = require('path');
// In newer Node.js versions where process is already global this isn't necessary.
var process = require("process");

var dir = "./";

// Loop through all the files in the temp directory
fs.readdir(dir, function (err, files) {
    if (err) {
        console.error("Could not list the directory.", err);
        process.exit(1);
    }

    files.forEach(function (file, index) {
        if (file.substring(file.length - 4) === 'json' && index === 0) {
            console.log(file);

            fs.readFile(file, 'utf8', function (err, data) {
                if (err)
                    return console.log(err);


                const d1 = data.substring(0, data.indexOf('labels'));
                const d2 = data.substring(data.indexOf('labels'));

                // console.log(d1.substring(d1.length - 30) + d2.substring(0, 30));

                const d3 = d2.replace(/time/g, 'start');

                fs.writeFile(file, d1 + d3, 'utf8', function (err) {
                    if (err) return console.log(err);
                });
            });
        }
    });
});