export const SET_ZOOM = 'SET_ZOOM';

export function setZoom(zoom) {
    return {
        type: SET_ZOOM,
        payload: zoom
    };
}
