export const ADD_SESSIONS = 'ADD_SESSIONS';
export const MODIFY_LABEL = 'MODIFY_LABEL';
export const ADD_LABEL = 'ADD_LABEL';
export const REMOVE_LABEL = 'REMOVE_LABEL';

export function addSessions(sessions) {
    return {
        type: ADD_SESSIONS,
        payload: sessions
    };
}

export function modifyLabel(sessionID, label) {
    return {
        type: MODIFY_LABEL,
        payload: {sessionID, label}
    };
}

export function addLabel(sessionID, label) {
    return {
        type: ADD_LABEL,
        payload: {sessionID, label}
    };
}

export function removeLabel(sessionID, label) {
    return {
        type: REMOVE_LABEL,
        payload: {sessionID, label}
    };
}