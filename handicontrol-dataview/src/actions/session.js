export const SET_SESSION = 'SET_SESSION';
export const CLEAR_SESSION = 'CLEAR_SESSION';

// /!\ Don't forget to add actions in the include list of /reducers/undo-config.js if the action must be undoable

export function setSession(session) {
    return {
        type: SET_SESSION,
        payload: session
    };
}

export function clearSession() {
    return {
        type: CLEAR_SESSION,
    };
}
