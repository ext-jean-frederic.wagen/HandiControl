import React, { Component } from "react";
import SessionsList from "./SessionsList";

export default class Sidebar extends Component {
    render() {
        return (
            <div className="sidebar">
                <SessionsList/>
            </div>
        );
    }
}
