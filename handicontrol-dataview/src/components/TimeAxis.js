import React, { Component } from "react";
import { connect } from "react-redux";
import { PX_PER_SECOND } from "../App";
import { findSession } from "../services/utils";

class TimeAxis extends Component {
    render() {
        const step = (1 / this.props.zoom * 1.5) | 0 || 1;
        const n = (this.props.end - this.props.start) / 1000 / step + 1 | 0;
        const padMs = (this.props.end - this.props.start) / 1000 - (n - 1) * step;
        const padding = padMs * PX_PER_SECOND * this.props.zoom;

        return (
            <div ref="timelineContainer" className="time-axis" style={{ paddingRight: padding }}>
                {[...Array(n)].map((ignored, sec) => {
                    sec *= step;
                    return (
                        <span className="time-axis--tick" key={sec}>
                        <span className="time-axis--label">
                            {sec ? (sec / 60 | 0) + ':' + ('0' + sec % 60).slice(-2) : 0}
                        </span>
                    </span>
                    );
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        zoom: state.present.zoom,
        start: state.present.session,
        end: findSession(state, state.present.session).end,
    };
};

export default connect(mapStateToProps)(TimeAxis);