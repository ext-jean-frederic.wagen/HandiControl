import React, {Component} from "react";
import Chart from 'chart.js';
import {connect} from "react-redux";
import {findSession} from "../services/utils";

class HeartRate extends Component {
    rendered = false;
    chart;

    init() {
        if (!this.rendered) return;

        this.chart = new Chart(this.refs.canvas, {
            type: 'line',
            data: {
                datasets: [{
                    data: this.props.data,
                    label: 'Battements cardiaques',
                    fill: false
                }]
            },
            options: {
                devicePixelRatio: 1,
                responsive: true,
                maintainAspectRatio: false,
                color: ['#673ab7'],
                legend: {display: false},
                elements: {
                    point: {
                        backgroundColor: '#9a67ea',
                    },
                    line: {
                        backgroundColor: '#673ab7',
                        borderColor: '#b398ea',
                        borderDash: [10, 10],
                        cubicInterpolationMode: 'monotone',
                    }
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            tooltipFormat: 'HH:mm:ss',
                            min: this.props.start,
                            max: this.props.end,
                        },
                        scaleLabel: {
                            display: false,
                        },
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: false,
                        },
                        gridLines: {
                            drawBorder: false,
                            display: false
                        }
                    }]
                },
            }
        });
    }

    update() {
        if (!this.rendered) return;

        // update options
        this.chart.options.scales.xAxes[0].time.min = this.props.start;
        this.chart.options.scales.xAxes[0].time.max = this.props.end;

        // update data
        this.chart.data.datasets[0].data = this.props.data;

        this.chart.update();
    }

    componentDidMount() {
        this.rendered = true;
        this.init();
    }

    render() {
        if (this.props.data && this.props.data.length && Array.isArray(this.props.data))
            this.update();

        return (
            <div className="data-module heartrate pt-2 position-relative">
                <canvas ref="canvas" height="100"/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const session = findSession(state, state.present.session);
    const data = session.heartRateStream.map(heartrate => ({
        x: heartrate.time,
        y: heartrate.heartRate,
    }));

    return {
        data,
        start: state.present.session,
        end: session.end,
    };
};

export default connect(mapStateToProps)(HeartRate);