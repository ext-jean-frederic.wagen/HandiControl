import React, { Component } from "react";
import { PX_PER_SECOND } from "../App";
import { connect } from "react-redux";
import store from "../store";
import { setZoom } from "../actions/zoom";
import { findSession } from "../services/utils";

const ZOOM_MIN = .1;
const ZOOM_MAX = 2;
const MAX_CANVAS_SIZE = 32767; // px

class ModulesBox extends Component {
    constructor(props) {
        super(props);

        this.onWheel = this.onWheel.bind(this);
    }

    componentDidMount() {
        this.refs.modulesBox.addEventListener('wheel', this.onWheel);
    }

    componentWillUnmount() {
        this.refs.modulesBox.removeEventListener('wheel', this.onWheel);
    }

    onWheel(e) {
        if (!e.ctrlKey) return;

        // canvas size is limited to 32767px = 2^15-1
        const zoomMax = Math.min(ZOOM_MAX,
            MAX_CANVAS_SIZE / (this.props.end - this.props.start) * 1000 / PX_PER_SECOND
        );

        const zoom = Math.max(
            Math.min(
                this.props.zoom - (e.deltaY / 1000),
                zoomMax
            ),
            ZOOM_MIN
        );
        store.dispatch(setZoom(zoom));
    }

    render() {
        const timeWidthStyle = {
            width: (this.props.end - this.props.start) / 1000 * PX_PER_SECOND * this.props.zoom,
        };

        return (
            <div>
                <div ref="modulesBox" className="modules-box">
                    <div className="modules-box--inner" style={{ ...timeWidthStyle }}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        zoom: state.present.zoom,
        start: state.present.session,
        end: findSession(state, state.present.session).end,
    };
};

export default connect(mapStateToProps)(ModulesBox);
