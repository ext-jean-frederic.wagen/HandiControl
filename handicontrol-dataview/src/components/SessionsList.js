import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import store from '../store';
import { displayDateTime } from "../services/utils";
import { setSession } from "../actions/session";
import NoContent from "./NoContent";
import FilesService from "../services/FilesService";

class SessionsList extends Component {
    selectSession(session) {
        store.dispatch(setSession(session));
    }

    render() {
        if (!this.props.sessions.length)
            return (
                <NoContent text={(
                    <span>
                        Aucune session trouvée
                        <button className="btn btn-light mt-3" onClick={() => FilesService.getInstance().changeDirectory()}>
                            <FontAwesomeIcon icon="folder-open"/> Choisir un dossier
                        </button>
                    </span>
                )}/>
            );
        else
            return (
                <div className="list-group list-group-flush">
                    {this.props.sessions
                        .map(session => {
                            let className = 'list-group-item list-group-item-action flex-column align-items-start';
                            className += this.props.currentSessionID && this.props.currentSessionID === session.sessionID ? ' active' : '';
                            return (
                                <a href="#" className={className} key={session.sessionID}
                                   onClick={() => this.selectSession(session)}>
                                    <div className="d-flex w-100 justify-content-between">
                                        <h6 className="mb-1">{displayDateTime(session.sessionID)}</h6>
                                    </div>
                                    <p className="mb-0">{session.labels.length + (session.labels.length > 1 ? ' labels' : ' label')}</p>
                                </a>
                            );
                        })}
                </div>
            );
    }
}


const mapStateToProps = (state) => {
    return {
        sessions: state.present.sessions,
        currentSessionID: state.present.session
    };
};

export default connect(mapStateToProps)(SessionsList);