import React, { Component } from "react";

export default class Loading extends Component {
    render() {
        return (
            <div className="loading d-flex align-items-center justify-content-center">
                <div className="spinner-border text-primary" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );
    }
}
