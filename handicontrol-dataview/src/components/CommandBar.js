import React, { Component } from "react";
import { ActionCreators } from 'redux-undo';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import FilesService from "../services/FilesService";
import store from "../store";
import { connect } from "react-redux";
import { isDirty } from "../services/utils";

class CommandBar extends Component {
    render() {
        return (
            <div className="command-bar">
                <button className="btn btn-light btn-sm m-1" disabled={!this.props.canSave}
                        onClick={() => FilesService.getInstance().save()}>
                    <FontAwesomeIcon icon="save"/>
                </button>
                <button className="btn btn-light btn-sm m-1"
                        onClick={() => FilesService.getInstance().changeDirectory()}>
                    <FontAwesomeIcon icon="folder-open"/>
                </button>
                <div className="border-left border-right m-1"/>
                <button className="btn btn-light btn-sm m-1" disabled={!this.props.canUndo}
                        onClick={() => store.dispatch(ActionCreators.undo())}>
                    <FontAwesomeIcon icon="undo-alt"/>
                </button>
                <button className="btn btn-light btn-sm m-1" disabled={!this.props.canRedo}
                        onClick={() => store.dispatch(ActionCreators.redo())}>
                    <FontAwesomeIcon icon="redo-alt"/>
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        canSave: isDirty(state),
        canUndo: state.past.length > 0,
        canRedo: state.future.length > 0,
    };
};

export default connect(mapStateToProps)(CommandBar);