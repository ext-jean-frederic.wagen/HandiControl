import React, {Component} from "react";
import Vis from 'vis/dist/vis-timeline-graph2d.min';
import 'vis/dist/vis-timeline-graph2d.min.css';
import {connect} from "react-redux";
import {findSession, generateID} from "../services/utils";
import store from "./../store";
import {addLabel, modifyLabel, removeLabel} from "../actions/sessions";


class Timeline extends Component {
    $timeline;
    options;
    timelineItemsDataSet;

    constructor(props) {
        super(props);

        this.updateLabelName = this.updateLabelName.bind(this);
        this.updateLabelTime = this.updateLabelTime.bind(this);
        this.addListeners = this.addListeners.bind(this);

        this.options = {
            start: new Date(this.props.start),
            end: new Date(this.props.end),
            editable: true,
            timeAxis: {
                scale: 'second',
                step: 1
            },
            showMajorLabels: false,
            showMinorLabels: false,
            align: 'left',
            margin: 10,
            zoomable: false,
            selectable: true,
            moveable: false,
            template: (item, element, data) => {
                if (item == null) return;
                return '<div id="' + item.id
                    + '" class="itemCE" contenteditable="true">'
                    + item.content + '</div>';
            },
            onAdd: (item, callback) => {
                item.content = "Nouveau";
                item.id = generateID(item.start.getTime());
                let label = {
                    id: item.id,
                    sessionId: this.props.start,
                    start: item.start.getTime(),
                    labelId: 0,//TODO
                    labelName: item.content
                };
                if (item.end) {
                    label.end = item.end.getTime();
                    callback(item);
                }
                store.dispatch(addLabel(this.props.start, label));

            },
            onMove: (item, callback) => {
                this.updateLabelTime(item);
            },
            onRemove: (item, callback) => {
                let label = JSON.parse(JSON.stringify(this.props.labels.find(label => label.id === item.id)));
                if (!label) return;
                store.dispatch(removeLabel(this.props.start, label));
            },
        };
    }

    updateLabelName(element) {
        let label = this.props.labels.find(label => label.id === element.id);
        if (!label) return;
        let newLabel = JSON.parse(JSON.stringify(label));
        newLabel.labelName = element.innerHTML;
        if (label.labelName === newLabel.labelName) return;
        store.dispatch(modifyLabel(this.props.start, newLabel));
    }

    updateLabelTime(item) {
        let label = this.props.labels.find(label => label.id === item.id);
        if (!label) return;
        let newLabel = JSON.parse(JSON.stringify(label));
        newLabel.start = item.start.getTime();
        if (item.end) newLabel.end = item.end.getTime();
        store.dispatch(modifyLabel(this.props.start, newLabel));
    }

    addListeners(element) {
        element.addEventListener('click', (event) => {
            document.activeElement.blur();
            element.focus();
            document.execCommand('selectAll', false, null);
            event.stopPropagation();
        }, false);
        element.addEventListener("focusout", () => {
            this.updateLabelName(element);
        }, false);
        element.addEventListener("keydown", (event) => {
            if (event.keyCode === 13) {
                this.updateLabelName(element);
            }
        });
    }

    init() {
        const {
            labels,
            start,
            end,
        } = this.props;

        this.options.start = new Date(start);
        this.options.end = new Date(end);

        let timelineItems = [];
        labels.forEach((label) => {
            let timelineItem = {};
            timelineItem.start = new Date(label.start);
            timelineItem.id = label.id;
            timelineItem.content = label.labelName;
            if (label.end) timelineItem.end = new Date(label.end);
            timelineItems.push(timelineItem);
        });

        this.$timeline.setOptions(this.options);

        this.timelineItemsDataSet.clear();
        this.timelineItemsDataSet.add(timelineItems);

        let allCE = document.getElementsByClassName("itemCE");
        for (let i = 0; i < allCE.length; i++) {
            this.addListeners(allCE[i]);
        }
    }

    componentDidMount() {
        const {timelineContainer} = this.refs;
        this.timelineItemsDataSet = new Vis.DataSet([]);
        this.$timeline = new Vis.Timeline(timelineContainer, this.timelineItemsDataSet, this.options);
        this.init();

        document.getElementById('timeline').onclick = function () {
            if (document.activeElement.classList.contains("itemCE")) document.activeElement.blur();
            window.getSelection().removeAllRanges();
        }
    }

    componentWillUnmount() {
        this.$timeline.destroy();
    }

    render() {
        if (this.props.labels && Array.isArray(this.props.labels) && this.$timeline) {
            this.init();
        }

        return (
            <div id="timeline" ref="timelineContainer" className="data-module"/>
        );
    }
}

const mapStateToProps = (state) => {
    const session = findSession(state, state.present.session);
    return {
        labels: session.labels,
        start: state.present.session,
        end: session.end,
    };
};

export default connect(mapStateToProps)(Timeline);