import React, { Component } from "react";
import Draggable from 'react-draggable';
import { PX_PER_SECOND } from "../App";
import TimeAxis from "./TimeAxis";
import { connect } from "react-redux";

class TimeSlider extends Component {
    noScroll = false;
    isDragging = false;
    mouseDelta = 0;

    constructor(props) {
        super(props);

        this.state = {
            x: 0,
        };

        this.clickHandler = this.clickHandler.bind(this);
        this.onDragStop = this.onDragStop.bind(this);
        this.onDragStart = this.onDragStart.bind(this);
        this.onDrag = this.onDrag.bind(this);
    }

    clickHandler(e) {
        if (e.target === this.refs.slider || e.target === this.refs.sliderElement || e.target === this.refs.sliderLine)
            return;

        const newX = e.clientX - this.refs.wrapper.getBoundingClientRect().left;

        this.noScroll = true;

        this.props.onUpdate(newX / PX_PER_SECOND / this.props.zoom);
    }

    onDragStart(e) {
        this.isDragging = true;
        this.mouseDelta = e.clientX - e.target.getBoundingClientRect().left;
    }

    onDrag(e) {
        const newX = e.x + this.refs.wrapper.parentNode.parentNode.scrollLeft - 250 - this.mouseDelta + 8;

        this.props.onUpdate(newX / PX_PER_SECOND / this.props.zoom);
    }

    onDragStop(e) {
        const newX = e.x + this.refs.wrapper.parentNode.parentNode.scrollLeft - 250 - this.mouseDelta + 8;

        this.noScroll = true;
        this.isDragging = false;
        this.refs.slider.style.transform = 'translate(' + newX + 'px,0px)';

        this.props.onUpdate(newX / PX_PER_SECOND / this.props.zoom);

        return false;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.refs.slider && !this.noScroll)
            this.refs.slider.scrollIntoView({ behavior: "auto", block: "center", inline: "center" });
        this.noScroll = false;
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return !this.isDragging;
    }

    render() {
        const position = {
            x: this.props.time * PX_PER_SECOND * this.props.zoom,
            y: 0,
        };

        return (
            <div className="data-module time-slider-wrapper" onMouseDown={this.clickHandler} ref="wrapper">
                <Draggable axis="x" bounds="parent" position={position} handle=".time-slider-element"
                           onStop={this.onDragStop} onStart={this.onDragStart} onDrag={this.onDrag}>
                    <div ref="slider" className="time-slider" style={{}}>
                        <div ref="sliderElement" className="time-slider-element"/>
                        <div ref="sliderLine" className="time-slider-line"/>
                    </div>
                </Draggable>
                <TimeAxis/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        zoom: state.present.zoom,
    };
};

export default connect(mapStateToProps)(TimeSlider);