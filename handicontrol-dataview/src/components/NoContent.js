import React, { Component } from "react";

export default class NoContent extends Component {
    render() {
        return (
            <div className="no-content">
                <div>
                    {this.props.text}
                </div>
            </div>
        );
    }
}
