import React, { Component } from "react";
import Plyr from 'plyr';
import plyrIcons from 'plyr/dist/plyr.svg';

export default class Video extends Component {
    track;
    textTrackCues = [];

    constructor(props) {
        super(props);

        this.setSubtitles = this.setSubtitles.bind(this);
        this.onTimeUpdate = this.onTimeUpdate.bind(this);
    }

    componentDidMount() {
        this.refs.video.addEventListener('timeupdate', this.onTimeUpdate, true);
        this.refs.video.addEventListener('loadedmetadata', this.setSubtitles);

        new Plyr(this.refs.video, {
            debug: true,
            controls: ['play', 'progress', 'current-time', 'mute', 'volume', 'captions'],
            seekTime: 3,
            invertTime: false,
            captions: { active: true, language: 'en', update: true },
            fullscreen: { enabled: false },
            iconUrl: plyrIcons,
        });
    }

    componentWillUnmount() {
        this.refs.video.removeEventListener('timeupdate', this.onTimeUpdate);
        this.refs.video.removeEventListener('loadedmetadata', this.setSubtitles);
    }

    onTimeUpdate() {
        this.props.onUpdate(this.refs.video.currentTime);
    }

    setTime(time) {
        this.refs.video.currentTime = time;
    }

    setSubtitles() {
        if (this.track) {
            // remove old text-tracks items
            for (let cue of this.textTrackCues)
                this.track.removeCue(cue);
            this.textTrackCues = [];
        } else {
            this.track = this.refs.video.addTextTrack("captions", "English", "en");
            this.track.mode = "showing";
        }

        for (let label of this.props.labels) {
            const start = (label.start - this.props.start) / 1000;
            const end = label.end ? (label.end - this.props.start) / 1000 : start + 3;
            const cue = new VTTCue(start, end, ' ' + label.labelName + ' ');
            this.textTrackCues.push(cue);
            this.track.addCue(cue);
        }
    }

    componentDidUpdate(oldProps) {
        if (JSON.stringify(oldProps.labels) !== JSON.stringify(this.props.labels))
            this.setSubtitles();
    }

    render() {
        return (
            <div className="flex-grow-1">
                <video ref="video" preload="auto" controls controlsList="nodownload nofullscreen"
                       src={this.props.videoSrc}>
                </video>
            </div>
        );
    }
}
