import { CLEAR_SESSION, SET_SESSION } from "../actions/session";

export function session(state = false, action) {
    switch (action.type) {
        case SET_SESSION:
            return action.payload.sessionID;
        case CLEAR_SESSION:
            return false;
        default:
            return state;
    }
}
