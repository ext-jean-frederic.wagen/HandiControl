import {ADD_LABEL, ADD_SESSIONS, MODIFY_LABEL, REMOVE_LABEL} from "../actions/sessions";

export function sessions(state = [], action) {
    switch (action.type) {
        case ADD_SESSIONS:
            return action.payload;

        case MODIFY_LABEL : {
            let newState = JSON.parse(JSON.stringify(state));
            const session = findSession(newState, action.payload.sessionID);

            let newLabel = action.payload.label;
            let label = session.labels.find((label) => label.id === newLabel.id);
            if (!label
                || (label.labelName === newLabel.labelName
                && label.start === newLabel.start
                && (!label.end || label.end === newLabel.end))){
                return state;
            }

            label.labelName = action.payload.label.labelName;
            label.start = action.payload.label.start;
            if (action.payload.label.end) label.end = action.payload.label.end;
            return newState;
        }

        case REMOVE_LABEL: {
            let newState = JSON.parse(JSON.stringify(state));
            const session = findSession(newState, action.payload.sessionID);
            session.labels.splice(session.labels.findIndex(label => label.id === action.payload.label.id), 1);
            return newState;
        }

        case ADD_LABEL: {
            let newState = JSON.parse(JSON.stringify(state));
            const session = findSession(newState, action.payload.sessionID);
            session.labels.push(action.payload.label);
            return newState;
        }

        default:
            return state;
    }
}

function findSession(state, id) {
    return state.find(session => session.sessionID === id);
}