import { combineReducers } from 'redux';
import undoable from 'redux-undo';
import { sessions } from "./sessions";
import { session } from "./session";
import { zoom } from "./zoom";
import { loading } from "./loading";
import { undoConfig } from "./undo-config";

// Access state by state.present.sub-state;
export default undoable(
    combineReducers({
        sessions,
        session,
        zoom,
        loading,
    }),
    undoConfig
);