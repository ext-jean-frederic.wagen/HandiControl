import { SET_ZOOM } from "../actions/zoom";

export function zoom(state = 1, action) {
    switch (action.type) {
        case SET_ZOOM:
            return action.payload;
        default:
            return state;
    }
}
