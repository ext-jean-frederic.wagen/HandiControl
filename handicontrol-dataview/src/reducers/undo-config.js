import {includeAction} from "redux-undo";
import {SET_SESSION} from "../actions/session";
import {ADD_LABEL, REMOVE_LABEL, MODIFY_LABEL} from "../actions/sessions";

export const undoConfig = {
    limit: 100,
    filter: includeAction([SET_SESSION, REMOVE_LABEL, ADD_LABEL, MODIFY_LABEL]),
    debug: process.env.NODE_ENV === 'development',
};
