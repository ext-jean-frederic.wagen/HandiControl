const BrowserWindow = require('electron').BrowserWindow;
const Menu = require('electron').Menu;
const ipcMain = require('electron').ipcMain;

const menuTemplate = [
    {
        label: 'Fichier',
        submenu: [
            {
                label: 'Enregistrer',
                enabled: false,
                accelerator: 'CmdOrCtrl+S',
                click: function (menuItem, focusedWin) {
                    const focusedWindow = BrowserWindow.getFocusedWindow();
                    focusedWin.webContents.send('save');
                }
            },
            {
                label: 'Enregistrer sous...',
                accelerator: 'CmdOrCtrl+shift+S',
                click: function (menuItem, focusedWin) {
                    const focusedWindow = BrowserWindow.getFocusedWindow();
                    focusedWin.webContents.send('save-as');
                }
            },
            { type: 'separator' },
            {
                label: 'Ouvrir...',
                accelerator: 'CmdOrCtrl+O',
                click: function (menuItem, focusedWin) {
                    const focusedWindow = BrowserWindow.getFocusedWindow();
                    focusedWin.webContents.send('open-dir');
                }
            },
        ],
    },
    {
        label: 'Édition',
        submenu: [
            {
                label: 'Annuler',
                enabled: false,
                accelerator: 'CmdOrCtrl+Z',
                click: function (menuItem, focusedWin) {
                    const focusedWindow = BrowserWindow.getFocusedWindow();
                    focusedWin.webContents.send('undo');
                }
            },
            {
                label: 'Rétablir',
                enabled: false,
                accelerator: 'CmdOrCtrl+Y',
                click: function (menuItem, focusedWin) {
                    const focusedWindow = BrowserWindow.getFocusedWindow();
                    focusedWin.webContents.send('redo');
                }
            }
        ],
    },
];

if (process.env.ELECTRON_ENV === 'development') {
    menuTemplate.push({
        label: 'Dev',
        submenu: [
            { role: 'toggleDevTools' },
            { role: 'reload' },
            { role: 'forceReload' },
            { role: 'toggleDevTools' },
            { role: 'toggleFullScreen' },
        ],
    });
}

const menu = Menu.buildFromTemplate(menuTemplate);

ipcMain.on('save-enabled', (event, enabled) => menu.items[0].submenu.items[0].enabled = enabled);
ipcMain.on('undo-enabled', (event, enabled) => menu.items[1].submenu.items[0].enabled = enabled);
ipcMain.on('redo-enabled', (event, enabled) => menu.items[1].submenu.items[1].enabled = enabled);

module.exports.menu = menu;
