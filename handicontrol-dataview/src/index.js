import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import moment from "moment";
import store from "./store";
import App from './App';
import './style/main.scss';
import 'babel-polyfill';
import "moment/locale/fr-ch";
import './services/icons';
import './services/globalEvents';

moment.locale('fr-ch');

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);
