import React, { Component } from 'react';
import { connect } from "react-redux";
import { ToastContainer } from "react-toastify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Video from "./components/Video";
import Sidebar from "./components/Sidebar";
import Timeline from "./components/Timeline";
import HeartRate from "./components/HeartRate";
import TimeSlider from "./components/TimeSlider";
import NoContent from "./components/NoContent";
import ModulesBox from "./components/ModulesBox";
import CommandBar from "./components/CommandBar";
import Loading from "./components/Loading";
import './App.css';
import { findSession } from "./services/utils";

export const PX_PER_SECOND = 45;

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            time: 0
        };

        this.onVideoUpdate = this.onVideoUpdate.bind(this);
        this.onSliderUpdate = this.onSliderUpdate.bind(this);
    }

    onVideoUpdate(time) {
        this.setState({ time });
    }

    onSliderUpdate(time) {
        this.refs.video.setTime(time);
    }

    renderData() {
        return (
            <div className="content">
                <div className="flex-grow-1 d-flex data-module">
                    <Video ref="video" onUpdate={this.onVideoUpdate} labels={this.props.session.labels}
                           start={this.props.session.sessionID} videoSrc={this.props.session.videoSrc}/>
                </div>
                <ModulesBox start={this.props.session.sessionID} end={this.props.session.end}>
                    <TimeSlider time={this.state.time} onUpdate={this.onSliderUpdate}/>

                    {this.props.session.heartRateStream.length ? <HeartRate/> : null}

                    <Timeline/>
                </ModulesBox>
            </div>
        );
    }

    renderNoData() {
        return (
            <NoContent text={
                <span>
                    <FontAwesomeIcon icon="long-arrow-alt-left" size="lg"></FontAwesomeIcon> Choisissez une session à charger
                </span>}/>
        );
    }

    render() {
        const content = this.props.session ? this.renderData() : this.renderNoData();

        return (
            <div className="root">
                <CommandBar/>
                <div className="d-flex flex-grow-1">
                    <Sidebar/>
                    {content}
                </div>
                <ToastContainer hideProgressBar autoClose={2500}/>
                {this.props.loading ? <Loading/> : null}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.present.loading,
        session: findSession(state, state.present.session),
    };
};

export default connect(mapStateToProps)(App);