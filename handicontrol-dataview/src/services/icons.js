import { library } from '@fortawesome/fontawesome-svg-core'
import { faFolderOpen, faLongArrowAltLeft, faRedoAlt, faSave, faUndoAlt } from '@fortawesome/free-solid-svg-icons'

library.add(faSave);
library.add(faFolderOpen);
library.add(faLongArrowAltLeft);
library.add(faUndoAlt);
library.add(faRedoAlt);