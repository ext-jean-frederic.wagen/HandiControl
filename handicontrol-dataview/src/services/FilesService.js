const fs = window.require('fs-extra'); // Load the File System to execute our common tasks (CRUD)
const path = require('path');
const remote = window.require('electron').remote;
const { dialog } = window.require('electron').remote;

const ffprobe = remote.getGlobal('ffprobe');
const ffprobeStaticPath = remote.getGlobal('ffprobeStaticPath');

import { ActionCreators } from 'redux-undo';
import { toast } from "react-toastify";
import store from "./../store";
import { addSessions } from "../actions/sessions";
import { clearSession } from "../actions/session";
import { setLoading } from "../actions/loading";
import { generateID, isDirty } from "./utils";

export default class FilesService {
    static instance;

    workDir = false;
    saveDir = false;

    static getInstance() {
        if (FilesService.instance == null)
            FilesService.instance = new FilesService();

        return this.instance;
    }

    constructor() {
        this.changeDirectory = this.changeDirectory.bind(this);
        this.choose = this.choose.bind(this);
        this.load = this.load.bind(this);
        this.save = this.save.bind(this);
        this.saveAs = this.saveAs.bind(this);
        this.askSaveDir = this.askSaveDir.bind(this);
        this.writeSessionToFile = this.writeSessionToFile.bind(this);
    }

    async load(files) {
        let sessions = [];

        for (let filename of files) {
            const data = await fs.readFile(filename, 'utf-8');
            const session = JSON.parse(data);

            const videoPath = await getVideoPath(path.dirname(filename), session.sessionID);
            if (!videoPath) continue;

            const videoInfo = await ffprobe(videoPath, { path: ffprobeStaticPath });
            const duration = videoInfo.streams[0].duration;

            session.labels.forEach((label) => {
                let id = "";
                do {
                    id = generateID(label.start);
                } while (session.labels.some((l) => l.id === id));
                label.id = id;
            });
            session.end = session.sessionID + duration * 1000;
            session.videoSrc = videoPath;
            sessions.push(session);


        }

        store.dispatch(addSessions(sessions));
    }

    async changeDirectory() {
        if (isDirty(store.getState()))
            dialog.showMessageBox({
                    type: "question",
                    title: 'Avant d\'ouvrir un nouveau dossier...',
                    message: 'Voulez-vous sauvegarder ?',
                    buttons: ['Oui', 'Non'],
                    cancelId: 1,
                },
                async response => {
                    if (response === 0) await this.save();
                    this.choose();
                });
        else
            this.choose();
    }

    async choose() {
        dialog.showOpenDialog({
                title: 'Choisir les données',
                properties: ['openDirectory']
            },
            async filenames => {
                if (filenames === undefined) {
                    console.log("No directory selected");
                    return;
                }

                store.dispatch(clearSession());
                store.dispatch(setLoading(true));

                this.workDir = filenames[0];
                console.log('workdir', this.workDir);

                try {
                    let files = await fs.readdir(this.workDir);
                    files = files
                        .filter(file => file.split('.').pop() === 'json')
                        .map(file => this.workDir + path.sep + file);

                    await this.load(files);
                } catch (e) {
                    console.error(e);
                    dialog.showErrorBox('Erreur lors du chargement des données', e.message)
                } finally {
                    store.dispatch(setLoading(false));
                    store.dispatch(ActionCreators.clearHistory());
                }
            });
    }

    async saveAs() {
        return this.askSaveDir() ? await this.save() : false;
    }

    askSaveDir() {
        const filenames = dialog.showOpenDialog({
            title: 'Choisir le dossier pour l\'enregistrement',
            buttonLabel: 'Enregistrer',
            defaultPath: this.workdir,
            properties: ['openDirectory'],
        });

        if (filenames === undefined)
            return false;

        this.saveDir = filenames[0];

        return true;
    }

    async save() {
        if (!this.saveDir && !this.askSaveDir())
            return;

        const response = dialog.showMessageBox({
            type: "question",
            title: 'Confirmation d\'enregistrement',
            message: 'Êtes-vous sûrs ?\nL\'enregistrement écrasera les fichiers existants.',
            buttons: ['Oui', 'Non'],
            cancelId: 1,
        });

        if (response !== 0) return;

        store.dispatch(setLoading(true));

        // Check if the file is writable.
        const accessError = fs.accessSync(this.saveDir, fs.constants.W_OK);

        if (accessError) {
            dialog.showErrorBox('Erreur lors de l\'enregistrement', 'Le dossier sélectionné n\'a pas d\'accès en écriture.');
            store.dispatch(setLoading(false));
            return;
        }

        const sessions = store.getState().present.sessions;

        let promises = [];
        for (let session of sessions) {
            let newSession = JSON.parse(JSON.stringify(session));
            if (newSession.labels.length > 0)
                newSession.labels.forEach((label) => delete label.id);

            promises.push(this.writeSessionToFile(newSession));
        }

        return Promise.all(promises)
            .then(() => toast.success('Enregistrement terminé.'))
            .catch((e) => console.error(e) || dialog.showErrorBox('Erreur lors de l\'enregistrement', e.message))
            .finally(() => store.dispatch(setLoading(false)));
    }

    writeSessionToFile(session) {
        const { end, videoSrc, ...data } = session;

        const json = JSON.stringify(data);

        const filePath = this.saveDir + path.sep + session.sessionID + '.json';

        return fs.writeFile(filePath, json);
    }
}

async function getVideoPath(path, id) {
    let files = await fs.readdir(path);

    // search in files
    const videoFiles = files
        .filter(file => file.split('.').pop() === 'mp4')
        .filter(file => Number(file.split('.').shift()) === id);

    if (videoFiles.length) return path + '/' + videoFiles[0];


    // Search in subfolders
    const directories = [];
    for (const file of files)
        if ((await fs.stat(path + '/' + file)).isDirectory())
            directories.push(file);

    for (const dir of directories) {
        const result = await getVideoPath(path + '/' + dir, id);
        if (result) return result;
    }

    return false;
}
