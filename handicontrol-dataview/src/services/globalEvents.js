import FilesService from "./FilesService";
import store from "../store";
import { ActionCreators } from "redux-undo";
import { isDirty } from "./utils";

const ipcRenderer = window.require('electron').ipcRenderer;

ipcRenderer.on('save', () => FilesService.getInstance().save());
ipcRenderer.on('save-as', () => FilesService.getInstance().saveAs());
ipcRenderer.on('open-dir', () => FilesService.getInstance().changeDirectory());
ipcRenderer.on('undo', () => store.dispatch(ActionCreators.undo()));
ipcRenderer.on('redo', () => store.dispatch(ActionCreators.redo()));

store.subscribe(() => {
    const state = store.getState();

    const canSave = isDirty(state);
    const canUndo = state.past.length > 0;
    const canRedo = state.future.length > 0;

    ipcRenderer.send('save-enabled', canSave);
    ipcRenderer.send('undo-enabled', canUndo);
    ipcRenderer.send('redo-enabled', canRedo);
});
