import moment from "moment";

export function firstUppercase(s) {
    return s[0].toUpperCase() + s.substr(1);
}

export function toMoment(s) {
    return moment(s, 'DD.MM.YYYY');
}

export function displayDateTime(timestamp) {
    return moment(timestamp).format('D MMMM YYYY, HH:mm');
}

export function todayInRange(d1, d2) {
    return toMoment(d1).isSameOrBefore(moment()) && toMoment(d2).isSameOrAfter(moment());
}

/* filter past to check if two entry with same sessionID are following,
   then that means that this session has been modified */
export function isDirty(state) {
    return state.past
        .concat([state.present])
        .map(pastState => pastState.session)
        .filter(session => !!session)
        .filter((session, i, array) => i ? session === array[i - 1] : false)
        .filter((session, i, array) => array.indexOf(session) === i)
        .length > 0;
}

export function findSession(state, id) {
    return state.present.sessions.find(session => session.sessionID === id);
}

export function generateID(start) {
    const min = 1;
    const max = 10000;
    let newId = min + Math.random() * (max - min);
    return "" + start + newId;
}