HandiControl is an open source to help polyhandicapped persons and their caretakers discover, acquire and use her/his best possible autonomy.  

Have a look at https://handicontrolapp.wixsite.com/website (in French for now)

Who is interested in participating to the development, advising us, financing, ... (r)evolutionnary ways to enable autonomous actions by polyhandicapped persons ?

The handicontrol folder contains the project's outcomes related to the monitoring system. The handicontrol-dataview folder is a desktop application to label the video. On the handicontrol-ML folder you can find preliminary work related to machine learning.

Discover the documents ... and let us know.

Jean-Frederic.Wagen@hefr.ch
Professor HES-SO
University of Applied Sciences and Arts of Western Switzerland
HEIA-FR - Pérolles 80 - PO BOX  32 - CH-1705 Fribourg
+41 26 429 65 53

Thanks to our contributors:
Ms. Luisa Defanti, 
Ms. Rita Caridade, 
Ms. Dominique Dubé, 
Mr. Jonathan Donzallaz, 
Mr. Yann-Ivain Beffa, 
Mr. Bruno Tschopp, 
Mr. Karl Daher, 
Prof. Dr. Jean Hennebert, 
and
Ms. Aurélie Hertig <aurelie.hertig@students.hevs.ch>, 
Ms. Justine Mariaux <justine.mariaux@students.hevs.ch>, 
Ms. Raquel Benvindo Tavares <raquel.benvindotavares@students.hevs.ch>
Prof. Christophe Boulé <christophe.boule@hevs.ch>,