import { createStackNavigator, createAppContainer } from "react-navigation";
import Home from "./screens/Home";
import Control from "./screens/Control";
import Labels from "./screens/Labels";

const AppNavigator = createStackNavigator(
    {
        Home: {
            screen: Home
        },
        Control: {
            screen: Control
        },
        Labels: {
            screen: Labels
        }
    },
    {
        initialRouteName: "Home",
        headerMode: 'none',
    }
);

export default createAppContainer(AppNavigator);