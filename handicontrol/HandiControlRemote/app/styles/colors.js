const primaryColor: string = '#673ab7'; // deep purple 500
const primaryLightColor: string = '#9a67ea'; // light
const primaryDarkColor: string = '#320b86'; // dark
const primaryColorTransparent: string = primaryColor + '88';
const primaryDarkColorTransparent: string = primaryDarkColor + '88';

const secondaryColor: string = '#fdd835'; // yellow 600
const secondaryLightColor: string = '#ffff6b'; // light
const secondaryDarkColor: string = '#c6a700'; // dark

const dividerColor: string = '#e1e8ee';

export {
    primaryColor,
    primaryLightColor,
    primaryDarkColor,
    primaryColorTransparent,
    primaryDarkColorTransparent,
    secondaryColor,
    secondaryLightColor,
    secondaryDarkColor,
    dividerColor,
};