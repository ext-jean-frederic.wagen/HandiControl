// React Native Elements theme - useless till version 1.0.0 (next)
import { primaryColor, primaryColorTransparent, secondaryColor } from "./colors";

const elementsTheme = {
    primary: primaryColor,
    secondary: secondaryColor,
    Button: {
        buttonStyle: {
            backgroundColor: primaryColorTransparent,
        },
        icon: { type: 'ionicon' },
    },
};

export { elementsTheme };