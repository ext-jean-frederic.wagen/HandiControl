import { primaryColor, primaryColorTransparent, primaryDarkColor, primaryDarkColorTransparent } from "./colors";

const buttonColor: string = primaryColor;
const buttonColorTransparent: string = primaryColorTransparent;
const buttonDarkColorTransparent: string = primaryDarkColorTransparent;

const buttonStyle = {
    color: primaryColor,
};

const toolbarStyle = {
    backgroundColor: primaryColor,
    height: 56,
    elevation: 4, // 4dp
};

const statusBarStyle = {
    backgroundColor: primaryDarkColor,
};

const iconType = 'ionicon';

const iconProps = {
    style: { marginRight: 0 },
    type: iconType,
};

const iconWithTextProps = {
    type: iconType,
};

export {
    buttonStyle,
    buttonColor,
    buttonColorTransparent,
    buttonDarkColorTransparent,
    toolbarStyle,
    statusBarStyle,
    iconProps,
    iconWithTextProps
};