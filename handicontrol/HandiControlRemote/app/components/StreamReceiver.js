/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { NodePlayerView } from 'react-native-nodemediaclient';


type Props = {
    pairedIP: string
};
export default class StreamReceiver extends Component<Props> {
    serverIp;
    nodePlayerView: NodePlayerView;

    constructor(props: Props) {
        super(props);

        this.serverIp = props.pairedIP;
        this.state = {
            ip: props.pairedIP
        }
    }

    start() {
        this.nodePlayerView.start();
    }

    stop() {
        this.nodePlayerView.stop();
    }

    render() {
        return (
            <View style={{ top: 0, left: 0, bottom: 0, right: 0, position: "absolute", zIndex: -1, flex: 1 }}>
                <NodePlayerView
                    style={{ flex: 1, backgroundColor: '#333' }}
                    ref={(nodePlayerView) => {
                        this.nodePlayerView = nodePlayerView
                    }}
                    inputUrl={"rtsp://" + this.state.ip + ":5540/live/stream"}
                    scaleMode={"ScaleAspectFit"}
                    bufferTime={300}
                    maxBufferTime={1000}
                    autoplay={false}
                />
            </View>
        );
    }
}

