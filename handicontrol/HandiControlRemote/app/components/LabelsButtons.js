import React, { Component } from 'react';
import { StyleSheet, ToastAndroid, View, TouchableNativeFeedback } from 'react-native';
import { Button } from "react-native-elements";
import store from 'react-native-simple-store';
import { buttonColorTransparent, buttonDarkColorTransparent } from "../styles/components";
import StreamSyncService from "../services/StreamSyncService";

type Props = {
    sessionID: number,
    labels: [],
};
type State = {
    pressed: []
};
export default class LabelsButtons extends Component<Props, State> {
    labelsPress: {} = {};

    constructor(props: Props) {
        super(props);

        this.saveLabel = this.saveLabel.bind(this);
        this.handlePressIn = this.handlePressIn.bind(this);
        this.handlePressOut = this.handlePressOut.bind(this);

        this.state = {
            pressed: []
        };
    }

    async saveLabel(label: {}, start: number, end: number = 0) {
        const delay: number = StreamSyncService.getInstance().streamDelay;

        const sessionLabel: {} = {
            sessionID: this.props.sessionID,
            start: start - delay,
            labelID: label.id,
            labelName: label.name,
        };

        if (end) sessionLabel.end = end - delay;

        await store.push('session-labels', sessionLabel);

        ToastAndroid.show('Labellisé comme "' + label.name + '"', ToastAndroid.SHORT);
        console.log('Label enregistré', label.name, start, (end ? end - start : 0) + ' ms');
    }

    async handlePressIn(label) {
        this.labelsPress[label.id] = Date.now();
        let pressed = this.state.pressed;
        pressed[label.id] = true;
        await this.setState({ pressed });
    }

    async handlePressOut(label) {
        const end: number = Date.now();
        const start: number = this.labelsPress[label.id];

        if (end - start > 1000)
            this.saveLabel(label, start, end);
        else
            this.saveLabel(label, start);

        let pressed = this.state.pressed;
        pressed[label.id] = false;
        await this.setState({ pressed });
    }

    render() {
        return (
            <View>
                {this.props.labels.map((label) => (
                    <Button title={label.name}
                            buttonStyle={{ ...styles.marginTop, paddingVertical: 4 }}
                            {...buttonProps}
                            backgroundColor={this.state.pressed[label.id] ? buttonDarkColorTransparent : buttonColorTransparent}
                            key={label.id}
                            onPressIn={() => this.handlePressIn(label)}
                            onPressOut={() => this.handlePressOut(label)}
                    />
                ))}
            </View>
        );
    }
}

const buttonProps = {
    backgroundColor: buttonColorTransparent,
    containerViewStyle: {
        marginLeft: 0,
        marginRight: 0
    },
};

const styles = StyleSheet.create({
    marginTop: {
        marginTop: 10,
    },
});
