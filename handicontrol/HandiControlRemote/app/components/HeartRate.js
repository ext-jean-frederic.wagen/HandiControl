import React, { Component } from 'react';
import { View } from 'react-native';
import { Badge } from "react-native-elements";
import { primaryColorTransparent } from "../styles/colors";
import RemoteService, { DataPath } from "../services/RemoteService";

type Props = {};
type State = {
    heartRate: number
};
export default class HeartRate extends Component<Props, State> {
    remoteService: RemoteService;

    constructor(props: Props) {
        super(props);

        this.heartRateUpdate = this.heartRateUpdate.bind(this);

        this.state = {
            heartRate: 0
        };

        this.remoteService = RemoteService.getInstance();
    }

    heartRateUpdate(path: string, data: string) {
        this.setState({
            heartRate: parseInt(data, 10)
        });
    }

    componentDidMount(): void {
        this.remoteService.addCallback(DataPath.HEART_RATE_UPDATE_PATH, this.heartRateUpdate);
    }

    componentWillUnmount() {
        this.remoteService.removeCallback(DataPath.HEART_RATE_UPDATE_PATH, this.heartRateUpdate);
    }

    render() {
        return (
            <View>
                <Badge
                    value={this.state.heartRate}
                    containerStyle={{ backgroundColor: primaryColorTransparent }}
                />
            </View>
        );
    }
}