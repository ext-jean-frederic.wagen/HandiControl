import React, { Component } from 'react';
import { ProgressBarAndroid, StyleSheet, ToastAndroid, View } from 'react-native';
import { buttonColor, iconWithTextProps } from "../styles/components";
import { Button } from "react-native-elements";
import DataSyncService from "../services/DataSyncService";
import { primaryLightColor } from "../styles/colors";
import Margin from "./Margin";


type Props = {};
type State = {
    loading: boolean,
    progress: number,
};
export default class Sync extends Component<Props, State> {

    constructor(props: Props) {
        super(props);


        this.state = {
            loading: false,
            progress: 0,
        };

        this.sync = this.sync.bind(this);
        this.endSync = this.endSync.bind(this);
        this.onProgress = this.onProgress.bind(this);
    }

    componentDidMount(): void {
        DataSyncService.getInstance().addObserver(this.onProgress);
    }

    componentWillUnmount(): void {
        DataSyncService.getInstance().removeObserver(this.onProgress);
    }

    sync() {
        this.setState({
            loading: !this.state.loading
        });

        DataSyncService.getInstance()
            .upload()
            .then(() => {
                ToastAndroid.show("Synchronisé avec succès", ToastAndroid.SHORT);
                this.endSync();
            })
            .catch(e => {
                ToastAndroid.show("Serveur non atteignable", ToastAndroid.LONG); // TODO: Change to a banner
                this.endSync();
            });
    }

    onProgress(progress: number) {
        this.setState({ progress });
    }

    endSync() {
        this.setState({
            loading: false,
            progress: 0
        });
    }

    render() {
        return (
            <View>
                <Button title="Envoyer" buttonStyle={styles.button} backgroundColor={buttonColor}
                        large raised textStyle={styles.buttonText}
                        icon={{ name: 'md-cloud-upload', ...iconWithTextProps }}
                        containerViewStyle={styles.noMargin}
                        onPress={this.sync}
                        loading={this.state.loading}
                        disabled={this.state.loading}
                />

                <Margin/>

                {this.state.loading &&
                <ProgressBarAndroid
                    progress={this.state.progress}
                    indeterminate={false}
                    color={primaryLightColor}
                    styleAttr="Horizontal"
                />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: { paddingVertical: 12 },
    buttonText: { fontSize: 18 },
    noMargin: {
        marginLeft: 0,
        marginRight: 0
    }
});