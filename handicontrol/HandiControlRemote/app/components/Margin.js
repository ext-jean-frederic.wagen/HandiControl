import React, { Component } from 'react';
import { View } from 'react-native';

type Props = {
    horizontal?: boolean,
    size?: number
};
export default function Margin(props: Props) {
    const style: {} = props.horizontal ? {
        paddingLeft: props.size ? props.size : 10
    } : {
        paddingTop: props.size ? props.size : 10
    };

    return <View style={style}/>;
}