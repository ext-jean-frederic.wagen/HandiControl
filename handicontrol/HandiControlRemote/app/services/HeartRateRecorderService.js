import RemoteService, { DataPath } from "./RemoteService";

type HeartRate = {
    heartRate: number,
    time: number,
};

export default class HeartRateRecorderService {
    static instance: HeartRateRecorderService;

    static getInstance(): HeartRateRecorderService {
        if (HeartRateRecorderService.instance == null)
            HeartRateRecorderService.instance = new HeartRateRecorderService();

        return this.instance;
    }

    remoteService: RemoteService;
    heartRateStream: Array<HeartRate> = [];

    constructor() {
        this.init = this.init.bind(this);
        this.destroy = this.destroy.bind(this);
        this.onHeartRateUpdate = this.onHeartRateUpdate.bind(this);
        this.getHeartRateStream = this.getHeartRateStream.bind(this);

        this.remoteService = RemoteService.getInstance();
    }

    init(){
        this.heartRateStream = [];
        this.remoteService.addCallback(DataPath.HEART_RATE_UPDATE_PATH, this.onHeartRateUpdate);
    }

    destroy(){
        this.remoteService.removeCallback(DataPath.HEART_RATE_UPDATE_PATH, this.onHeartRateUpdate);
    }

    async onHeartRateUpdate(path: string, data: string) {
        this.heartRateStream.push({
            heartRate: parseInt(data, 10),
            time: Date.now(),
        })
    }

    getHeartRateStream(): Array<HeartRate> {
        return this.heartRateStream;
    }
}
