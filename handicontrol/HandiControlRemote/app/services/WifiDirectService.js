import { NetworkInfo } from 'react-native-network-info';
import WifiDirect from 'react-native-wifi-direct';
import SocketsService from './SocketsService';
import { ToastAndroid } from 'react-native';


export default class WifiDirectService {

    connected = false;
    isOwner = false;
    myIp;
    pairedDeviceIp;
    sService;
    onConnectedCallback;

    constructor() {
    }

    connect(onConnectedCallback) {
        this.onConnectedCallback = onConnectedCallback;
        this.addWifiDirectListener();
        WifiDirect.initialize();
        WifiDirect.discoverPeers().then((success) => {
            if (success) {
                console.log("Peer discovery has initiated successfully.")
            } else {
                console.log("Peer discover failed to initiate. Is your Wi-Fi on?")
            }
        });
    }

    disconnect() {
        this.removeWifiDirectListener();
        WifiDirect.disconnect().then((success) => {
            if (success) {
                console.log("Disconnecting initiated.")
            } else {
                console.log("Disconnect initiation failed.  Are you already disconnected?")
            }
        });
        this.connected = false;
    }

    peersUpdated = (event) => {
        event.devices.map(device => {
            if (!this.connected) {
                if (/*device.device == "Android_4789" ||*/ device.device == "Android_f553" || device.device == "[Phone] Samsung Galaxy S7") {

                    console.log(device);
                    WifiDirect.connect(device.address).then((success) => {
                        if (success) {
                            console.log("Connection has initiated.");
                        } else {
                            console.log("Connection failed to initiate.");
                        }
                    });
                }
            }
        });
    };

    connectionInfoUpdated = (event) => {
        this.connected = true;
        NetworkInfo.getIPV4Address(ip => {
            ToastAndroid.show('Owner ' + event.address + ' IP: ' + ip, ToastAndroid.LONG);
            this.myIp = ip;
            this.sService = SocketsService.getInstance();
            if (ip == event.address) {
                this.isOwner = true;
                this.sService.startServer(this.callbackOnMessage.bind(this));
            } else {
                this.isOwner = false;
                this.pairedDeviceIp = event.address;
                setTimeout(() => {
                    this.sService.startClient(event.address, this.callbackConnectedToServer.bind(this), () => {
                        },
                        this.callbackOnMessage.bind(this));
                },100);
            }
        });
    };

    callbackConnectedToServer() {
        this.sService.sendMessage(this.myIp);
        this.onConnectedCallback(this.pairedDeviceIp);
        //this.sService.closeSockets();
    }

    callbackOnMessage(message) {
        this.pairedDeviceIp = message;
        //this.sService.closeSockets();
        ToastAndroid.show('Client -> Server : ' + message, ToastAndroid.LONG);
        this.onConnectedCallback(this.pairedDeviceIp);
    }

    addWifiDirectListener() {
        WifiDirect.addListener('PEERS_UPDATED', this.peersUpdated);
        WifiDirect.addListener('CONNECTION_INFO_UPDATED', this.connectionInfoUpdated);
    }

    removeWifiDirectListener() {
        WifiDirect.removeListener('PEERS_UPDATED', this.peersUpdated);
        WifiDirect.removeListener('CONNECTION_INFO_UPDATED', this.connectionInfoUpdated);
    }
}