import store from 'react-native-simple-store';

export default class LabelsService {
    static instance: LabelsService;

    static getInstance(): LabelsService {
        if (LabelsService.instance == null)
            LabelsService.instance = new LabelsService();

        return this.instance;
    }

    constructor() {
        store.get(labelsKey).then(labels => {
            if (!labels)
                store.save(labelsKey, initialLabels);
        });
    }

    async getAll() {
        return await store.get(labelsKey);
    }

    async deleteAll() {
        await store.delete(labelsKey);
    }

    async delete(id: number) {
        const labels = await store.get(labelsKey);
        const newLabels = labels.filter(label => label.id !== id);
        await store.save(labelsKey, newLabels);
        return newLabels;
    }

    async save(id: number, name: string) {
        if (id > -1) await this.update(id, name);
        else await this.create(name);
    }

    async update(id: number, name: string) {
        let labels = await store.get(labelsKey);
        labels = labels.map(label => {
            if (label.id === id)
                return Object.assign({}, label, { name: name });
            return label;
        });
        await store.save(labelsKey, labels);
    }

    async create(name) {
        let labelsNextId = await store.get(labelsCurrentIdKey) + 1;
        if (labelsNextId === null)
            labelsNextId = 0;
        await store.save(labelsCurrentIdKey, labelsNextId);
        await store.push(labelsKey, {
            id: labelsNextId,
            name
        })
    }
}

type Label = {
    id: number,
    name: string
};

const initialLabels: Array<Label> = [
    {
        id: 0,
        name: 'Contentement'
    },
    {
        id: 1,
        name: 'Contrariété'
    },
    {
        id: 2,
        name: 'Autre'
    }
];
const labelsKey: string = 'labels';
const labelsCurrentIdKey: string = 'labelsCurrentId';