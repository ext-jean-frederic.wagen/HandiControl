import SocketsService from "./SocketsService";

export default class RemoteService {
    static instance: RemoteService;

    static getInstance(): RemoteService {
        if (RemoteService.instance == null)
            RemoteService.instance = new RemoteService();

        return this.instance;
    }

    socketsService: SocketsService;
    callbacks: Map<string, Set<Function>> = new Map();

    constructor() {
        this.messageCallback = this.messageCallback.bind(this);

        this.socketsService = SocketsService.getInstance();
        this.socketsService.addObserver(this.messageCallback)
    }

    messageCallback(message: string) {
        const pathSeparatorIndex: number = message.indexOf(pathSeparator);
        const path: string = pathSeparatorIndex > -1 ? message.split(pathSeparator)[0] : message;
        const data: string = pathSeparatorIndex > -1 ? message.substring(pathSeparatorIndex + 1) : '';

        console.log('path: ' + path);
        console.log('data: ' + data);

        const callbacks: Set<Function> = this.callbacks.get(path);

        if (callbacks) callbacks.forEach(callback => callback(path, data));
    }

    addCallback(path: string, callback: Function) {
        let callbacks = this.callbacks.get(path);
        if (callbacks instanceof Set) {
            callbacks.add(callback);
            this.callbacks.set(path, callbacks);
        } else
            this.callbacks.set(path, new Set([callback]));
    }

    removeCallback(path: string, callback: Function) {
        let callbacks = this.callbacks.get(path);
        if (callbacks instanceof Set && callbacks.size > 1) {
            callbacks.delete(callback);
            this.callbacks.set(path, callbacks);
        } else
            this.callbacks.delete(path);
    }

    sendOrder(order: string, data: string = false) {
        if (data !== false)
            this.socketsService.sendMessage(order + '/' + data);
        else
            this.socketsService.sendMessage(order);
    }

    send(order: string, data: string = false) {
        if (data !== false)
            this.socketsService.sendMessage(order + '/' + data);
        else
            this.socketsService.sendMessage(order);
    }
}

const pathSeparator: string = '/';

const Order = {
    START_RECORDING: 'START_RECORDING',
    STOP_RECORDING: 'STOP_RECORDING',
    PLAY_MUSIC: 'PLAY_MUSIC',
    PAUSE_MUSIC: 'PAUSE_MUSIC',
    NEXT_MUSIC: 'NEXT_MUSIC',
    VOLUME_UP: 'VOLUME_UP',
    VOLUME_DOWN: 'VOLUME_DOWN',
    BLACK_IMAGE: 'BLACK_IMAGE',
};

const DataPath = {
    HEART_RATE_UPDATE_PATH: 'heart_rate_update',
    BLACK_IMAGE_TIME: 'BLACK_IMAGE_TIME',
};

export { Order, DataPath };