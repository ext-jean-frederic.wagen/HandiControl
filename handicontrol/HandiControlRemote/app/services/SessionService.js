import HeartRateRecorderService from "./HeartRateRecorderService";

const RNFS = require('react-native-fs');
import store from 'react-native-simple-store';

export const SESSION_DIR_PATH: string = '/storage/emulated/0/Android/data/com.handicontrolremote/files/';

export default class SessionService {
    static instance: SessionService;

    static getInstance(): SessionService {
        if (SessionService.instance == null)
            SessionService.instance = new SessionService();

        return this.instance;
    }

    heartRateRecorder: HeartRateRecorderService;

    constructor() {
        this.heartRateRecorder = HeartRateRecorderService.getInstance();
        this.saveSession = this.saveSession.bind(this);
    }

    async saveSession(sessionID: number) {
        const path = SESSION_DIR_PATH + sessionID + '.json';

        let labels: [] = await store.get('session-labels');
        if (labels)
            labels = labels.filter(label => label.sessionID === sessionID);
        else
            labels = [];

        const heartRateStream = this.heartRateRecorder.getHeartRateStream();

        if (heartRateStream.length === 0 && labels.length === 0) return;

        const data: string = JSON.stringify({
            sessionID,
            heartRateStream: heartRateStream,
            labels,
        });

        RNFS.writeFile(path, data, 'utf8')
            .then(() => console.log('Session file written.'))
            .catch(console.error);
    }
}
