var RNFS = require('react-native-fs');
import FtpUtility from './../native/FtpUtility';
import { SESSION_DIR_PATH } from "./SessionService";

export default class DataSyncService {
    static instance: DataSyncService;

    static getInstance(): DataSyncService {
        if (DataSyncService.instance == null)
            DataSyncService.instance = new DataSyncService();

        return this.instance;
    }

    progress: number = 0;

    constructor() {
        this.upload = this.upload.bind(this);
    }

    upload() {
        this.progress = 0;
        this.notifyObservers();
        return new Promise((resolve, reject) => {
            RNFS.readDir(SESSION_DIR_PATH)
                .then(async (result) => {
                    await FtpUtility.config('handicontrol.tic.heia-fr.ch', 223, 'yann-ivain.beffa', 'La7keyoo'); // TODO: make it editable (form)

                    try {
                        const uploadedFiles = (await FtpUtility.list("sessions/")).split(',');

                        for (let file of result) {
                            if (!uploadedFiles.includes(file.name))
                                await FtpUtility.upload(file.path, "sessions/" + file.name);

                            this.progress += 1 / result.length;
                            this.notifyObservers();
                        }
                    } catch (e) {
                        reject(e);
                        return;
                    }

                    resolve(true);
                });
        });
    }

    observersFunctions: Set<Function> = new Set();

    notifyObservers() {
        for (const observer of this.observersFunctions)
            observer(this.progress);
    }

    addObserver(observer: Function) {
        this.observersFunctions.add(observer);
    }

    removeObserver(observer: Function) {
        this.observersFunctions.delete(observer);
    }
}