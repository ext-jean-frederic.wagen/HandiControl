import RemoteService, { DataPath, Order } from "./RemoteService";

const MIN_DELAY: number = 100;
const MAX_DELAY: number = 10000;

export default class StreamSyncService {
    static instance: StreamSyncService;

    static getInstance(): StreamSyncService {
        if (StreamSyncService.instance == null)
            StreamSyncService.instance = new StreamSyncService();

        return this.instance;
    }

    remoteService: RemoteService;
    startSyncTime: number = 0;
    blackImageTime: number = 0;
    streamDelay: number = 500;
    timeout;

    constructor() {
        this.remoteService = RemoteService.getInstance();

        this.remoteService.addCallback(DataPath.BLACK_IMAGE_TIME, (path, data) => this.onBlackImageTime(path, data));

        this.sync = this.sync.bind(this);
        this.onBlackImage = this.onBlackImage.bind(this);
        this.onBlackImageTime = this.onBlackImageTime.bind(this);
    }

    sync() {
        if (this.timeout)
            clearTimeout(this.timeout);

        this.blackImageTime = 0;
        this.startSyncTime = Date.now();

        this.remoteService.send(Order.BLACK_IMAGE);

        return new Promise((resolve, reject) => {
            this.timeout = setTimeout(() => {
                if (this.startSyncTime > 0) {
                    this.startSyncTime = 0;
                    this.blackImageTime = 0;
                    reject();
                } else
                    resolve();
            }, MAX_DELAY);
        });
    }

    onBlackImage() {
        const now: number = Date.now() - 100; // Subtract 100ms as reaction time
        const start: number = this.startSyncTime + 30; // Add 30ms as ping ~ time to send order
        const delay: number = now - start;

        if (delay > MIN_DELAY || delay < MAX_DELAY && this.blackImageTime > 0 && this.startSyncTime > 0) {
            this.streamDelay = delay;
            console.log('New stream delay:', delay);
        }

        this.startSyncTime = 0;
        this.blackImageTime = 0;
    }

    onBlackImageTime(path: string, data: string) {
        this.blackImageTime = parseInt(data, 10);
    }
}