/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { View, StatusBar } from 'react-native';
import AppContainer from "./AppContainer";
import { statusBarStyle } from "./styles/components";

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar backgroundColor={statusBarStyle.backgroundColor}/>
                <AppContainer/>
            </View>
        );
    }
}
