import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Divider, FormInput, FormLabel } from "react-native-elements";
import { ToolbarAndroid } from 'react-native-vector-icons/Ionicons';
import store from 'react-native-simple-store';
import { buttonColor, toolbarStyle } from "./../styles/components";
import { iconWithTextProps } from "../styles/components";
import { dividerColor, primaryLightColor } from "../styles/colors";
import Margin from "../components/Margin";
import Sync from "../components/Sync";

type Props = {};
export default class Home extends React.Component<Props> {

    static pairedIP = "";

    static navigationOptions = {
        title: 'HandiControl',
    };

    constructor(props) {
        super(props);
        this.onPressConnect = this.onPressConnect.bind(this);
        this.onWDConnected = this.onWDConnected.bind(this);
        this.setServerIp = this.setServerIp.bind(this);

        this.state = { wifiIP: "192.168." };
    }

    componentDidMount(): void {
        store.get('serverIP').then(serverIP => {
            if (serverIP) this.setState({ wifiIP: serverIP });
        });
    }

    setServerIp(serverIP: string) {
        this.setState({ wifiIP: serverIP });
        store.save('serverIP', serverIP);
    }

    onPressConnect() {
        this.props.navigation.navigate('Control', {
            serverIP: this.state.wifiIP
        });
    }

    onWDConnected(pairedIP) {
        this.pairedIP = pairedIP;
        setTimeout(() => {
            this.props.navigation.navigate('Control', {
                serverIP: this.pairedIP
            });
        }, 200);
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ToolbarAndroid title="HandiControl" titleColor="white" style={toolbarStyle}/>

                <View style={styles.container}>
                    <View style={{ width: 200 }}>
                        <FormLabel labelStyle={styles.noMargin}>Adresse IP</FormLabel>
                        <FormInput onChangeText={this.setServerIp}
                                   value={this.state.wifiIP}
                                   placeholder="Adresse IP"
                                   containerStyle={styles.noMargin}/>

                        <Button title="Connexion" buttonStyle={styles.button} backgroundColor={buttonColor}
                                large raised textStyle={styles.buttonText}
                                icon={{ name: 'md-videocam', ...iconWithTextProps }}
                                containerViewStyle={styles.noMargin}
                                onPress={this.onPressConnect}/>

                        <Margin size={20}/>
                        <Divider style={{ backgroundColor: dividerColor }}/>
                        <Margin size={20}/>

                        <Button title="Labels" buttonStyle={styles.button} backgroundColor={buttonColor}
                                large raised textStyle={styles.buttonText}
                                icon={{ name: 'md-pricetags', ...iconWithTextProps }}
                                containerViewStyle={styles.noMargin}
                                onPress={() => this.props.navigation.navigate('Labels')}/>

                        <Margin size={20}/>
                        <Divider style={{ backgroundColor: dividerColor }}/>
                        <Margin size={20}/>

                        <Sync/>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: { paddingVertical: 12 },
    buttonText: { fontSize: 18 },
    noMargin: {
        marginLeft: 0,
        marginRight: 0
    }
});
