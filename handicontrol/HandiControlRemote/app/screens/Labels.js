import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import { List, ListItem } from 'react-native-elements'
import { ToolbarAndroid } from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import LabelsService from "../services/LabelsService";
import { buttonColor, toolbarStyle } from "../styles/components";
import Modal from "react-native-modal";
import Margin from "../components/Margin";

type Props = {};
export default class Labels extends React.Component<Props> {
    labelsService: LabelsService;

    static navigationOptions = {
        title: 'Labels'
    };

    constructor(props: Props) {
        super(props);

        this.state = {
            labels: [],
            modalVisible: false,
            editLabelName: '',
            editLabelId: -1
        };

        this.update = this.update.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.openLabelEdition = this.openLabelEdition.bind(this);
        this.openLabelCreation = this.openLabelCreation.bind(this);
        this.saveLabel = this.saveLabel.bind(this);
        this.labelsService = LabelsService.getInstance();
    }

    componentDidMount() {
        this.update();
    }

    update() {
        this.labelsService.getAll().then(labels => {
            if (labels)
                this.setState({ labels });
        });
    }

    closeModal() {
        this.setState({
            editLabelId: -1,
            editLabelName: '',
            modalVisible: false,
        });
    }

    openLabelEdition(id: number, name: string) {
        this.setState({
            editLabelId: id,
            editLabelName: name,
            modalVisible: true,
        });
    }

    openLabelCreation() {
        this.setState({
            editLabelId: -1,
            editLabelName: '',
            modalVisible: true,
        });
    }

    async saveLabel() {
        await this.labelsService.save(this.state.editLabelId, this.state.editLabelName);
        this.closeModal();
        this.update();
    }

    async deleteLabel(id: number) {
        await this.labelsService.delete(id);
        this.update();
    }

    render() {
        return (
            <View style={styles.container}>
                <ToolbarAndroid title="Labels" titleColor="white" style={toolbarStyle} navIconName="md-arrow-round-back"
                                onIconClicked={this.props.navigation.goBack}/>

                <List containerStyle={{}}>
                    {this.state.labels.map((l) => (
                        <ListItem
                            key={l.id}
                            title={l.name}
                            containerStyle={{ paddingHorizontal: 15 }}
                            titleStyle={{ fontSize: 18 }}
                            onPress={() => this.openLabelEdition(l.id, l.name)}
                            rightIcon={{ name: 'delete', color: '#be000e' }}
                            onPressRightIcon={() => this.deleteLabel(l.id)}
                        />
                    ))}
                </List>

                <ActionButton buttonColor={buttonColor} onPress={this.openLabelCreation}
                              fixNativeFeedbackRadius={true}/>

                <Modal
                    isVisible={this.state.modalVisible}
                    onBackdropPress={this.closeModal}
                    onBackButtonPress={this.closeModal}
                    onSwipe={this.closeModal}
                    swipeDirection="left"
                    supportedOrientations={['portrait', 'landscape']}>
                    <View style={{
                        padding: 22,
                        backgroundColor: '#fff',
                        borderRadius: 4,
                        borderColor: "rgba(0, 0, 0, 0.1)"
                    }}>
                        <Text style={{ fontSize: 20 }}>
                            {this.state.editLabelId > -1 ? 'Renommer le label' : 'Nouveau label'}
                        </Text>
                        <Margin/>
                        <TextInput
                            style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={editLabelName => this.setState({ editLabelName })}
                            value={this.state.editLabelName}
                            placeholder="Nom du label"
                        />
                        <Margin/>
                        <View style={{ flexDirection: 'row', alignSelf: 'stretch' }}>
                            <View style={styles.buttonContainer}>
                                <Button
                                    onPress={this.closeModal}
                                    title="Fermer"
                                    color={buttonColor}
                                />
                            </View>
                            <Margin horizontal/>
                            <View style={styles.buttonContainer}>
                                <Button
                                    onPress={this.saveLabel}
                                    title="Sauver"
                                    color={buttonColor}
                                />
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    list: {
        flex: 1
    },
    buttonContainer: {
        flex: 1,
    }
});
