import React, { Component } from 'react';
import { StyleSheet, View, ToastAndroid, StatusBar, BackHandler } from 'react-native';
import { Button } from "react-native-elements";
import { buttonColorTransparent } from "./../styles/components";
import HeartRate from "../components/HeartRate";
import StreamReceiver from '../components/StreamReceiver';
import RemoteService, { Order } from "../services/RemoteService";
import SocketsService from "../services/SocketsService";
import LabelsService from "../services/LabelsService";
import { iconProps } from "../styles/components";
import SessionService from "../services/SessionService";
import HeartRateRecorderService from "../services/HeartRateRecorderService";
import LabelsButtons from "../components/LabelsButtons";
import StreamSyncService from "../services/StreamSyncService";

/*
 * TODO: on disconnect return to home + stop music
 * TODO: fix buttons size + icon in button (instead of txt)
 */

type State = {
    musicPlaying: boolean,
    labels: [],
    serverIP: string,
    synchronizing: boolean,
}
type Props = {};
export default class Control extends Component<Props, State> {
    remoteService: RemoteService;
    socketsService: SocketsService;
    streamReceiver: StreamReceiver;
    _didFocusSubscription;
    _willBlurSubscription;
    onBackButtonPressAndroid = () => true;
    sessionID: number;

    constructor(props) {
        super(props);

        this.state = {
            musicPlaying: false,
            labels: [],
            serverIP: this.props.navigation.getParam('serverIP', ''),
            synchronizing: false,
        };

        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
            BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );

        this.remoteService = RemoteService.getInstance();
        this.socketsService = SocketsService.getInstance();
        this.toggleMusic = this.toggleMusic.bind(this);
        this.sync = this.sync.bind(this);

        this.sessionID = Date.now();
        console.log('SessionID', this.sessionID);
    }

    componentDidMount(): void {
        LabelsService.getInstance().getAll().then(labels => {
            if (labels)
                this.setState({ labels });
        });

        this.setState({
            serverIP: this.props.navigation.getParam('serverIP', '')
        });

        this.streamReceiver.start();

        HeartRateRecorderService.getInstance().init();

        this.socketsService.startClient(this.props.navigation.getParam('serverIP', ''),
            () => {
                ToastAndroid.show('Connecté à l\'appareil de saisie', ToastAndroid.SHORT);
                this.remoteService.sendOrder(Order.START_RECORDING, this.sessionID);
            },
            () => {
                ToastAndroid.show('Impossible de se connecter. Vérifiez que l\'appareil de saisie est activé', ToastAndroid.LONG);
                this.props.navigation.goBack();
            },
            () => {
            },
            () => this.props.navigation.goBack()
        );

        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
    }

    async componentWillUnmount(): void {
        this.remoteService.sendOrder(Order.STOP_RECORDING);
        this.streamReceiver.stop();
        this.socketsService.closeSockets();
        await SessionService.getInstance().saveSession(this.sessionID);
        HeartRateRecorderService.getInstance().destroy();
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }

    toggleMusic() {
        this.remoteService.sendOrder(this.state.musicPlaying ? Order.PAUSE_MUSIC : Order.PLAY_MUSIC);
        this.setState({ musicPlaying: !this.state.musicPlaying });
    }

    async sync() {
        await this.setState({ synchronizing: !this.state.synchronizing });
        if (this.state.synchronizing) {
            StreamSyncService.getInstance().sync()
                .catch(() => this.setState({ synchronizing: false }));
        } else {
            StreamSyncService.getInstance().onBlackImage();
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#000"/>

                <StreamReceiver pairedIP={this.state.serverIP}
                                ref={streamReceiver => this.streamReceiver = streamReceiver}/>
                <View style={styles.controlContainer}>
                    <View style={{ marginBottom: 'auto' }}>
                        <Button title="Arrêter"
                                {...buttonProps}
                                onPress={() => this.props.navigation.goBack()}/>
                    </View>
                    <LabelsButtons labels={this.state.labels} sessionID={this.sessionID}/>
                </View>
                <View style={styles.controlContainer}>
                    <View style={{ marginBottom: 'auto', alignSelf: 'flex-end', width: 40 }}>
                        <HeartRate/>
                    </View>
                    <View style={{ flexDirection: 'row', alignSelf: 'stretch' }}>
                        <Button icon={{ name: 'md-volume-low', ...iconProps }}
                                backgroundColor={buttonColorTransparent}
                                containerViewStyle={{ flex: 1, ...buttonProps.containerViewStyle }}
                                onPress={() => this.remoteService.sendOrder(Order.VOLUME_DOWN)}/>
                        <Button icon={{ name: 'md-volume-high', ...iconProps }}
                                backgroundColor={buttonColorTransparent}
                                containerViewStyle={{ flex: 1, marginLeft: 10, marginRight: 0 }}
                                onPress={() => this.remoteService.sendOrder(Order.VOLUME_UP)}/>
                    </View>
                    <Button title={this.state.musicPlaying ? "Pause musique" : "Play musique"}
                            buttonStyle={styles.marginTop}
                            {...buttonProps}
                            onPress={this.toggleMusic}/>
                    <Button title="Musique suiv."
                            buttonStyle={styles.marginTop}
                            {...buttonProps}
                            onPress={() => this.remoteService.sendOrder(Order.NEXT_MUSIC)}/>
                    <Button title={this.state.synchronizing ? "Image noire" : "Synchroniser"}
                            buttonStyle={styles.marginTop}
                            {...buttonProps}
                            onPress={this.sync}/>
                </View>
            </View>
        );
    }
}

const buttonProps = {
    backgroundColor: buttonColorTransparent,
    containerViewStyle: {
        marginLeft: 0,
        marginRight: 0
    },
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        zIndex: 2
    },
    controlContainer: {
        justifyContent: 'flex-end',
        width: 170,
        padding: 10,
        zIndex: 2
    },
    marginTop: {
        marginTop: 10,
    },
});
