/** @format */

import { AppRegistry } from 'react-native';
import App from './app/App';
import { name as appName } from './app.json';

const dev: boolean = true;

if (!dev) {
    console.disableYellowBox = true;
    console.error = (error) => error.apply;
}

AppRegistry.registerComponent(appName, () => App);
