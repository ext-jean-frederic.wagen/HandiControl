package com.handicontrolremote.ftp;

import android.text.TextUtils;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.jcraft.jsch.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

public class FtpUtility extends ReactContextBaseJavaModule {
    private static final String TAG = "FTP_UTILITY";
    private String host = "";
    private int port = 21;
    private String username = "";
    private String password = "";

    public FtpUtility(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "FtpUtility";
    }

    @ReactMethod
    public void config(String host, int port, String username, String password, Promise promise) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        promise.resolve(true);
    }

    /**
     * @param sourcePath      if you are using android remember to remove "file://" and use only the relative path
     *                        example: "/storage/0/myfile.txt"
     * @param destinationPath example: "/var/www/remote/myfile.txt"
     */
    @ReactMethod
    public void upload(String sourcePath, String destinationPath, Promise promise) {
        new Thread(() -> {
            Session session = null;
            Channel channel = null;
            try {
                JSch ssh = new JSch();
                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no"); // TODO: make it better

                session = ssh.getSession(username, host, port);
                session.setPassword(password);
                session.setConfig(config);

                session.connect();
                channel = session.openChannel("sftp");
                channel.connect();

                ChannelSftp sftp = (ChannelSftp) channel;

                sftp.put(sourcePath, destinationPath);

                promise.resolve(true);
            } catch (JSchException e) {
                promise.reject("JSchException", e);
                e.printStackTrace();
            } catch (SftpException e) {
                promise.reject("SftpException", e);
                e.printStackTrace();
            } finally {
                if (channel != null)
                    channel.disconnect();
                if (session != null)
                    session.disconnect();
            }
        }).start();
    }

    /**
     * @param destinationPath example: "/var/www/remote/"
     */
    @ReactMethod
    public void list(String destinationPath, Promise promise) {
        new Thread(() -> {
            Session session = null;
            Channel channel = null;
            try {
                JSch ssh = new JSch();
                Properties config = new Properties();
                config.put("StrictHostKeyChecking", "no");

                session = ssh.getSession(username, host, port);
                session.setPassword(password);
                session.setConfig(config);

                session.connect();
                channel = session.openChannel("sftp");
                channel.connect();

                ChannelSftp sftp = (ChannelSftp) channel;

                Vector ls = sftp.ls(destinationPath);

                List<String> filenames = new LinkedList<>();
                for (Object lsEntry : ls)
                    filenames.add(((ChannelSftp.LsEntry) lsEntry).getFilename());

                promise.resolve(TextUtils.join(",", filenames));
            } catch (JSchException e) {
                promise.reject("JSchException", e);
                e.printStackTrace();
            } catch (SftpException e) {
                promise.reject("SftpException", e);
                e.printStackTrace();
            } finally {
                if (channel != null)
                    channel.disconnect();
                if (session != null)
                    session.disconnect();
            }
        }).start();
    }
}