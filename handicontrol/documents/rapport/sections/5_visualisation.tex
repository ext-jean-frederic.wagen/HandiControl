\section{Visualisation des données}

Afin de visualiser les données saisies par le système \hc, une nouvelle application est développée. Ce chapitre décrit sa conception et certaines parties de son implémentation. À nouveau, des notions complexes du Javascript et du développement Web sont utilisées et il est nécessaire de saisir les bases pour apprécier tous les détails de ce chapitre.

\subsection{Conception}
Les objectifs de cette application sont clairs. Elle doit permettre de visualiser de la meilleure manière toutes les données enregistrées: les vidéos, les labels et les battements cardiaques. De plus, il doit être possible d'éditer toutes les données des labels.

Les appareils ciblés sont les ordinateurs standard, l'application est donc complètement distincte du système \hc présenté au chapitre \ref{implementation}. Cela permettra de travailler dans de bonnes conditions, éventuellement sur un grand écran pour avoir plus de données sur un même écran.

Les fonctionnalités prévues dans la version v1.0.0 étaient donc les suivantes :

\begin{itemize}
    \item Ouvrir et sauver les fichiers de données des sessions.
    \item Visualiser les labels et les battements cardiaques.
    \item Ajouter, éditer le texte et la position, et supprimer les labels.
    \item Voir la vidéo (son et image) de la session.
    \item Contrôler et visualiser facilement toute la durée de la session grâce à un bandeau avec une ligne du temps.
    \item Pouvoir annuler et rétablir toute action.
    \item Pouvoir zoomer et dézoomer dans le bandeau des données.
\end{itemize}

Une maquette a donc été réalisée afin de définir l'interface de l'application (figure \ref{fig:mockup-dataview}).

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=12cm]{img/dataview/mockup.jpg}
    \end{center}
    \caption{Maquette de l'application \hcd}
    \label{fig:mockup-dataview}
\end{figure}


\subsubsection{Langages et framework}
Afin de définir la technologie utilisée pour le développement de cette application, nous nous sommes aidés des contraintes. L'application doit pouvoir être utilisée sur tout système d'exploitation Windows, MacOS ou Linux. Le délai pour la première version est également à prendre en compte, et ne permet pas d'apprendre un nouveau langage.

Afin de pouvoir développer rapidement et sur tout système, nous avions comme choix Java (et JavaFX) ou Javascript (application web ou desktop). Étant donnée que nous n'avions jamais développé d'interface complexe avec Java, notre choix s'est porté sur Javascript.

Cependant, les applications web ne permettent pas d'accéder facilement au système de fichiers local de l'utilisateur. Nous avons donc choisi le framework Electron \cite{electronjs}.

Electron permet de \emph{développez des applications desktop multi-plateformes avec JavaScript, HTML et CSS}. Il est opensource et utilise Chromium (navigateur web opensource) pour afficher l'interface, comme pour une application web. Il est en plus possible d'utiliser toutes les fonctionnalités du runtime NodeJs pour lire des fichiers, traiter des vidéos, etc..

Nous avons également décidé d'utiliser \emph{React} pour construire l'interface utilisateur de l'application. Le framework est donc basé sur les mêmes concepts que ceux utilisés pour les applications mobiles \hc.

\subsection{Chargement et sauvegarde des données}
\label{Visualisation:data-loading}

Le stockage des données destinées au \ml n'étant pas optimal sur un serveur FTP, nous avons décidé dans un premier temps de charger les données depuis le système de fichier local. Ensuite, le but est de pouvoir ajouter d'autres \emph{Adapters} supportant différents systèmes pour charger et enregistrer les données. Par exemple, on pourrait ajouter un \emph{adapter} pour une base données \emph{MongoDB}, un adapter pour FTP, etc.. Cela devrait suivre le pattern \emph{Strategy}. Les \emph{adapters} doivent suivre l'interface suivante:

\begin{minted}{javascript}
class MyAdapter {
    // Ajoute les sessions dans le store Redux
    // Retourne une Promise
    changeDirectory() {
        // ...
        store.dispatch(addSessions(sessions));
    }
    
    // Récupère les sessions depuis le store redux et les enregistre
    // Retourne une Promise
    save() {
        const sessions = store.getState().present.sessions;
        // ...
    }

    // Enregistre également les sessions, en proposant de choisir la destination
    // Retourne une Promise
    saveAs() {
       // Demander la destination
       this.save();
    }
}
\end{minted}

Les objets \code{session} à ajouter au store doivent correspondre au format ci-dessous. Les données éventuellement absentes des fichiers générés par l'application \hcr doivent être calculées et ajoutées.

\begin{minted}{javascript}
const session = {
    sessionID: 1544113269323, // timestamp du début de session
    end: 1544113440000,       // timestamp de fin de session (à ajouter)
    videoSrc: "c:/data/videos/1544113269323.mp4", // chemin vers la vidéo
                                                  // (à ajouter)
    heartRateStream: [
        {
            heartRate: 96,      // battement cardiaque
            time: 1544113274392 // timestamp de la prise cette valeur
        },
    ],
    labels: [
        {
            start: 1544207085547, // timestamp du label
            end: 1544207089547,   // si label "range", timestamp de fin
            id: 15,               // id unique (dans la session) du label
                                  // (à ajouter)
            labelName: "Change"   // valeur du label
        }
    ]
}; 
\end{minted}


\subsubsection{Chargement et sauvegarde par le système de fichiers}

Lorsque l'utilisateur choisit un dossier, celui-ci est parcouru à la recherche de fichiers JSON. Le format de ceux-ci doit correspondre aux fichiers sauvegardés par l'application \hcr. Ensuite, une vidéo du même nom (le timestamp de la session) est recherchée dans le dossier et tous ses sous-dossiers.

Un id unique est généré pour chaque label, le chemin vers la vidéo est ajouté, et la fin de la session est calculée à partir de la durée de la vidéo. Le package \emph{ffprobe} \cite{ffprobe} est utilisé pour trouver cette information.

La sauvegarde effectue l'opération inverse, en écrivant le contenu des sessions dans des fichiers JSON, un par session, à l'emplacement choisi par l'utilisateur. L'enregistrement n'est possible que lorsqu'une modification a eu lieu, ce qui est vérifié par la présence d'actions dans l'historique (voir section \ref{dataview:internalState}).

\subsection{État interne des données}
\label{dataview:internalState}

Durant l'exécution de l'application, toutes les données sont stockées dans un objet global nommé \emph{store}. L'architecture utilisée est \emph{Flux} et est implémentée grâce à la librairie \emph{Redux} \cite{redux}. Le site de \emph{Redux} décrit l'architecture et l'implémentation en détail.

L'étude de cette architecture pourrait faire l'objet d'un cours, mais pour simplifier, trois concepts sont respectés. Le store est une source unique de vérité (1), l'état actuel est toujours disponible dans ce store, et importer des données dans le store permet d'actualiser les données de toute l'application. Le store est en lecture seule (2), la seule manière de modifier l'état est d'émettre une action. Ainsi, à chaque envoi d'action, tout l'application est mise à jour (figure \ref{fig:flux}). Enfin, les modifications sont faites par des fonctions pures (3), qui reçoivent en paramètre l'état (state) précédent et une action, et retourne un nouvel état.

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=12cm]{img/dataview/flux-archi.png}
    \end{center}
    \caption{Diagramme de l'architecture Flux}
    \source{\code{https://facebook.github.io/flux/docs/in-depth-overview.html}}
    \label{fig:flux}
\end{figure}

L'état dans le store de l'application est donc semblable à l'exemple suivant :

\begin{minted}{javascript}
{
    sessions: [
		{ /* session 1 */ },
		{ /* session 2 */ },
		// ...
		{ /* session n */ },
    ],
    session: 1544206613395,
    zoom: 1,
    loading: false
}
\end{minted}

La liste des sessions est stockée avec toutes les données de chacune d'elles. Les autres données sont l'identifiant de la session actuellement affichée, le niveau de zoom dans la zone des données, et l'état de l'application (en cours de chargement/sauvegarde ou non).

Le store ou les éléments du store nécessaires sont ensuite passés aux composants \emph{React} via les props. Quand une action est émise, le \emph{dispatcher} la dirige vers le \emph{reducer} approprié, qui crée un nouvel état. Celui-ci est stocké dans le store, et la vue est mise à jour avec les nouvelles données (figure \ref{fig:flux}).

\begin{minted}{javascript}
// Action pour changer de session (actions/session.js)
export const SET_SESSION = 'SET_SESSION';
export function setSession(session) {
    return {
        type: SET_SESSION,
        payload: session
    };
}

// Émission de l'action dans un composant (clic sur une nouvelle session)
//     (SessionsList.js)
store.dispatch(setSession(session));

// Le reducer (reducers/session.js)
export function session(state = false, action) {
    switch (action.type) {
        case SET_SESSION:
            return action.payload.sessionID;
            // ...
    }
}

// Les composants récupèrent les données et sont mis à jour (exemple: App.js)
const mapStateToProps = (state) => {
    return {
        // ...
        session: findSession(state, state.session),
    };
};
// et peuvent utiliser les données :
<Video /* ... */ labels={this.props.session.labels}
    start={this.props.session.sessionID}
    videoSrc={this.props.session.videoSrc}/>
\end{minted}

\subsubsection{Annuler, Rétablir}
Cette architecture a un énorme avantage, elle permet d'implémenter très facilement les actions annuler et rétablir. En effet, il est possible de sauver à chaque action l'état dans un tableau correspondant au passé. C'est ce que fait la librairie \emph{redux-undo} \cite{redux-undo} que nous avons utilisée. C'est une sorte de reducer d'ordre supérieur, qui englobe tous les reducers. Il ajoute les entrées \emph{past}, \emph{present} et \emph{future} dans le store. L'état actuel se trouve donc sous la clé \emph{present} comme ci-dessous:

\begin{minted}{js}
{
    past: [
        { /* état passé 1 */ }
        { /* état passé 2 */ }
        // ...
    ],
    present: { /* état courant */
        sessions: [ /* ... */ ],
        session: 1544206613395,
        zoom: 1,
        loading: false
    },
    future: [
        { /* état futur 1 */ }
        { /* état futur 2 */ }
        // ...
    ],
}
\end{minted}

On accède donc à une donnée de l'état présent en passant par la clé \emph{present} : \longcode{state.present.session}. Et on peut vérifier si l'action \emph{annuler} est possible en faisant \longcode{state.past.length > 0}. Les actions \emph{annuler} et \emph{rétablir} se font également en émettant des actions comme \longcode{store.dispatch(ActionCreators.undo())}. L'état présent est alors remplacé par le précédent et la vue est mise à jour.

\subsubsection{Synchronisation timeline-vidéo}
La ligne du temps (timeline) permet de changer la position actuelle de la vidéo par un glisser-déposer du curseur. Durant le glisser-déposer, la vidéo est également synchronisée, de sorte que l'on puisse observer toute une séquence afin de trouver le moment voulu de manière optimale.

La source de vérité pour la position actuelle de la vidéo (currentTime) se trouve dans l'élément \code{<video>}. Quand on bouge le curseur, une fonction du composant App est appelée, qui transmet le nouveau temps à une fonction du composant Video. Le \code{currentTime} de l'élément video est alors mis à jour. Cette dernière émet un évènement qui met à jour la valeur \code{time} dans l'état du composant App. Les \code{props} du composant TimeSlider sont alors mise à jour, et donc la vue et la position du curseur. Ce flux d'information est décrit dans la figure \ref{fig:time-sync}.

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=12cm]{img/dataview/time-update.jpg}
    \end{center}
    \caption{Synchronisation du temps entre le slider et la vidéo}
    \label{fig:time-sync}
\end{figure}

\subsection{Affichage des données}

Les données sont affichées par différents composants appelés \emph{modules} et situés dans le composant parent \code{ModulesBox} (hiérarchie décrite dans le code ci-dessous). Celui-ci gère la taille et le zoom sur cette zone. Il est possible de modifier le niveau de zoom grâce à la combinaison \emph{ctrl+molette haut/bas}.

\begin{minted}{html}
<ModulesBox>
    <TimeSlider/>
    <HeartRate/>
    <Timeline/>
</ModulesBox>
\end{minted}

Le zoom est limité par la largeur maximum de l'élément \code{<canvas>} utilisé pour le graphe des battements cardiaques. C'est une limite de 32767px ($2^{15} -1$). Au dessus de cette taille, les éléments ne sont pas dessinés dans le canvas. Ceci est une contrainte du moteur de rendu.

Si on désire ajouter un nouveau type de donnée (le graphe du son par exemple), il faudra simplement créer un nouveau composant et l'ajouter au \code{ModulesBox} avec une largeur de 100\%.

\subsection{Affichage et modification des labels}
Deux librairies ont été testées et explorées pour l'affichage des labels dans une barre du temps (timeline).
\begin{itemize}
    \item Timeline.js
    \item Vis.js
\end{itemize}
La librairie Vis.js a été choisie car elle comporte plus de fonctionnalités que Timeline.js.
De plus, la documentation de Vis.js est excellente ce qui a permis un développement simplifié.
Cette librairie permet également de sélectionner, ajouter, supprimer et modifier des "labels" dans la timeline.

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=\textwidth]{img/dataview/visJsTimeline.png}
    \end{center}
    \caption{Test local de la timeline}
\end{figure}

\subsubsection{Modification du texte d'un label} 
La librairie permet de modifier le texte et l'emplacement (par rapport à l'axe temps) d'un label s'il a été spécifié dans les options.
Pour les labels qui ont un début et une fin, la librairie prend également en compte le changement de la durée du label.
Dans la documentation de la librairie (http://visjs.org/docs/timeline/), ils nous suggèrent d'utiliser une popup pour demander le nouveau nom du label.
Cette implémentation est un peu lourde en nombre d'étapes que l'utilisateur doit faire.
L'utilisation des \emph{contenteditable} (attribut html permettant la modification du texte dans une zone) permet de modifier le label plus intuitivement qu'avec une popup.
La librairie fonctionne avec des évènements. Il faut donc donner des fonctions dans les paramètres de la librairie.
Nous avons donc donné une fonction qui s'occupe de la modification d'un label.

\subsubsection{Ajout et suppression d'un label} 
La librairie prend nativement en compte l'ajout et la suppression de labels.
En effet, si on active l'ajout et la suppression dans les options, il est possible de donner des méthodes de callback pour gérer ces évènements.
Par défaut, un petit bouton apparaît lors du clic sur un label permettant de supprimer celui-ci.
Et pour l'ajout, le comportement par défaut est un double clic sur l'axe du temps pour ajouter un label.
Ou un ctrl+drag'n'drop pour créer un label qui a un début et une fin !

\subsubsection{Difficultés d'implémentation}

Les principales difficultés n'ont pas été sur l'affichage de la \emph{timeline}, mais sur la modification, ajout et suppression de labels.
En effet, la librairie stocke par défaut les labels. Ce qui fait que nous nous retrouvons avec deux endroits où sont stockés les labels.
Pour éviter qu'ils apparaissent deux fois chacun, il ne faut pas retourner les callbacks à l'appelant. 
De plus, pour faire fonctionner la modification du nom avec l'utilisation des \emph{contenteditable}, il a fallu jouer avec les sélections des éléments pour éviter que des mauvais évènements ne soient déclenchés.

\subsection{Compilation}

La compilation du projet se fait grâce à des scripts npm. Ce sont des petits scripts à lancer en ligne de commande avec \code{npm run nomDuScript}.

Pour le mode développeur, il faut lancer deux scripts, \code{dev} et \code{start-dev}. Le premier lance la compilation du code javascript et des dépendances, tout en mettant à jour automatiquement lors de l'édition, tandis que le deuxième ouvre la fenêtre de l'application avec \emph{Electron}. Un script \code{unix-start-dev} est également disponible si l'on développe avec un système Unix.

Pour compiler une version production, il faut utiliser le script \code{win-dist}, \code{mac-dist} ou \code{linux-dist} en fonction du système visé. Le code est alors compilé, les ressources liées et une exécutable est créé. Il n'est possible de compiler pour MacOS que depuis MacOS lui-même. Afin de compiler pour Windows depuis un système Unix, \emph{Wine} est nécessaire. Compiler pour Linux depuis Windows est possible en passant par le sous-système Ubuntu par exemple \cite{wsl}.

\subsection{Application}
Le résultat atteint dans la première version est donc une application desktop cross-plateformes. Toutes les fonctionnalités prévues dans la phase de conception ont été implémentées. De plus, quelques autres fonctions ont été ajoutées :

\begin{itemize}
    \item Sous-titres sur la vidéo correspondant aux labels
    \item Raccourcis clavier pour sauvegarder, ouvrir un dosser, annuler, rétablir.
    \item Déplacement de la position actuelle par un glisser-déposer du curseur sur la timeline, avec les images de la vidéo synchronisées (se mettant à jour durant le déplacement).
\end{itemize}

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=\textwidth]{img/dataview/appv1.png}
    \end{center}
    \caption{Interface de l'application \hcd}
\end{figure}