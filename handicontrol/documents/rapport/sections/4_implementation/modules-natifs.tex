\subsection{Modules natifs}
\label{impl:native-modules}

Lorsqu'il est nécessaire de communiquer avec le système pour accéder à un de ses APIs, et que la librairie \rn ne propose pas déjà la fonction attendue, il est nécessaire de réaliser un module natif. La documentation de \rn explique parfaitement la création d'un module \cite{rn-modules}.

Pour simplifier, il faut créer 2 classes. L'une étend \code{ReactContextBaseJavaModule} et intègre le code du module. La méthode \code{getName()} doit être surchargée en retournant le nom du module, et les méthodes accessibles par le Javascript sont annotées avec \code{@ReactMethod}. La seconde classe implémente \code{ReactPackage} et sa méthode \code{createNativeModules()} renvoie une liste des modules du package. Il faut enfin référencer ce package dans le \code{getPackages()} de la classe \code{MainApplication}.

\begin{minted}{java}
public class FtpUtility extends ReactContextBaseJavaModule {
    // ...

    public FtpUtility(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "FtpUtility";
    }

    @ReactMethod
    public void upload(String sourcePath, String destinationPath, Promise promise) {
       // ...
    }
}

public class FtpPackage implements ReactPackage {
    // ...

    @Override
    public List<NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();
        modules.add(new FtpUtility(reactContext));
        return modules;
    }
}

// MainApplication.java
@Override
protected List<ReactPackage> getPackages() {
    return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            // other packages
            new FtpPackage()
    );
}
\end{minted}

On peut ensuite appeler une méthode native de la manière suivante dans le Javascript :

\begin{minted}{js}
NativeModules.FtpUtility.upload(file.path, "sessions/" + file.name);
\end{minted}

Pour envoyer une valeur de retour, la méthode native reçoit en paramètre un object \emph{Promise}. Il correspond à son équivalent Javascript et est utilisé pour faire un retour.

\begin{minted}{java}
// En cas de succès
promise.resolve(TextUtils.join(",", filenames));

// En cas d'erreur
promise.reject("JSchException", e);
\end{minted}

En Javascript, l'appel peut être géré comme un appel d'une promise standard, avec \emph{async/await} ou \emph{then/finally}:

\begin{minted}{js}
try {
    const uploadedFiles = (await FtpUtility.list("sessions/")).split(',');
    // ...
} catch (e) {
    // ...
}
\end{minted}

\subsubsection{Appel du natif vers Javascript}
Si un service ou toute méthode veut envoyer une valeur au Javascript, il est possible d'émettre des évènements côté natif :

\begin{minted}{java}
// Nouvel évènement émit avec un message sur le canal (path) 'heart_rate_update'
mReactContext
    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
    .emit('heart_rate_update', message);
\end{minted}

Et d'y souscrire côté Javascript :

\begin{minted}{js}
const observer = heartRate => console.log(heartRate);
DeviceEventEmitter.addListener('heart_rate_update', observer);
\end{minted}


\subsubsection{Composants UI Natifs}

Quand on veut intégrer des composants natifs dans l'UI, il est nécessaire d'utiliser l'API de \rn pour les \emph{Native UI Components} \cite{rn-ui-components}. A nouveau, la documentation permet d'obtenir rapidement un résulat. Nous l'avons utilisée pour intégrer la librairie \emph{rtmp-rtsp-stream-client-java} \cite{rtmp-rtsp-stream-client-java} permettant de \emph{streamer} depuis la caméra, qui nécessite un élément type \emph{SurfaceView} dans l'UI.

Notre classe principale doit être de type (\emph{extends}) \code{View} ou un de ses sous-types, mais n'a pas d'autre contrainte. La nôtre, \code{CameraView}, intègre la librairie de streaming. Un \emph{Manager} étendant \code{SimpleViewManager<T>} associé à notre classe permet la configuration:

\begin{minted}{java}
public class CameraViewManager extends SimpleViewManager<CameraView> {
    private static final int COMMAND_START_STREAM = 0;
    // ... (autres constantes)

    @Override // Définit le nom du composant
    public String getName() {
        return "CameraView";
    }

    @Override // Création du composant
    protected CameraView createViewInstance(ThemedReactContext reactContext) {
        return new CameraView(reactContext);
    }

    @Nullable
    @Override // Liste les méthodes disponibles (nom et identifiant)
    public Map<String, Integer> getCommandsMap() {
        MapBuilder.Builder<String, Integer> builder = MapBuilder.builder();
        return builder
                .put("startStream", COMMAND_START_STREAM)
                // ... (autres méthodes)
                .build();
    }

    @Override // Gère l'action lors de la réception d'un appel de méthode
    public void receiveCommand(CameraView cameraView, int commandId,
            @Nullable ReadableArray args) {
        switch (commandId) {
            case COMMAND_START_STREAM:
                cameraView.startStream(args.getString(0));
                break;
            // ... (autres méthodes)
        }
    }
}
\end{minted}

Cette fois, dans une classe correspondant au nouveau package et implémentant \code{ReactPackage}, il faut ajouter le manager dans la liste des \code{ViewManagers} associés. Cette classe est également à ajouter dans \code{MainApplication.java}.

\begin{minted}{java}
@Override
public List<ViewManager> createViewManagers(
        ReactApplicationContext reactContext) {
    List<ViewManager> managers = new ArrayList<>();
    managers.add(new CameraViewManager());
    return managers;
}
\end{minted}

Côté Javascript, il est intéressant de créer un \emph{wrapper} pour le composant créé, comme suit. Cela rend son utilisation plus aisée.

\begin{minted}{js}
const RCTCameraView = requireNativeComponent('CameraView', CameraView);

class CameraView extends Component {
    // ... (autres méthodes)

    startStream(streamUrl: string) { // Méthode disponible sur le composant
        UIManager.dispatchViewManagerCommand( // Appel de la méthode native
            findNodeHandle(this.refs['CameraView']),   // Référence
            UIManager.CameraView.Commands.startStream, // Identifiant
            [streamUrl]                                // Paramètre(s)
        );
    }

    render() { // Rendu du composant
        return <RCTCameraView ref={'CameraView'} {...this.props} />;
    };
}

CameraView.name = 'CameraView'; // Nom du composant
CameraView.propTypes = { // Propriétés disponibles
    cameraFace: PropTypes.bool,
    ...View.propTypes
};
\end{minted}

Enfin, on peut l'utiliser comme tout autre composant \rn :

\begin{minted}{js}
// Appel d'une méthode
this.cameraView.startStream(STREAM_URL);
// Rendu dans le jsx
<CameraView/>
\end{minted}