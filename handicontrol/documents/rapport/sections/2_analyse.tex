\section{Analyse}
Ce chapitre traite de l'analyse du vocabulaire et des capteurs. Cette phase est nécessaire à la conception d'un système utile et utilisable (\emph{usefulness \& usability}).

\subsection{Vocabulaire}
M. F. communique à travers des mimiques du visage, des bruits et des gestes des membres supérieurs.
Son vocabulaire permet tout à fait à ses accompagnants de comprendre lorsqu'il apprécie quelque chose ou au contraire lorsque quelque chose le dérange.
Le tableau ci-dessous résume son vocabulaire, le contexte et les capteurs qui permettent de détecter et/ou comprendre ce signal.
Des labels sont également associés à ces signaux. Ils correspondent à l'émotion ou le sentiment compris par l'accompagnateur. Ces labels seront utiles dans le futur lors de l'entraînement du système de \ml, afin qu'il puisse reconnaître automatiquement les sentiments à tel ou tel moment. 

\begin{longtable}{|p{2.4cm}|p{2.8cm}|p{2.7cm}|p{1.65cm}|p{3cm}|}
\hline
\textbf{Label} & \textbf{Signaux observables} & \textbf{Contexte} & \textbf{Capteurs} & \textbf{Feedback} \\ \hline
\endfirsthead
%
\multicolumn{5}{c}%
{{\bfseries Suite du tableau de la page précédente}} \\
\hline
\textbf{Label} & \textbf{Signaux observables} & \textbf{Contexte} & \textbf{Capteurs} & \textbf{Feedback} \\ \hline
\endhead
%
J’apprécie & Claquement de langue & Pendant la stimulation & Micro & La stimulation continue \\ \hline
Je veux encore & Claquement de langue & Si nous arrêtons la stimulation, il redemande d’avoir encore & Micro & Nous redémarrons la stimulation \\ \hline
Content & Rire et expression faciale joyeuse & Pendant la stimulation & Micro, Caméra & La stimulation continue \\ \hline
    & Expression faciale joyeuse ou souriante & Pendant la stimulation & Caméra & La stimulation continue \\ \hline
Mécontentement & Grognements et tension au niveau des membres supérieures du corps et expression faciale maussade & Pendant la stimulation & Micro, Caméra & La stimulation est arrêtée \\ \hline
    & Crispation au niveau des membres et expression faciale maussade & Pendant la stimulation & Camera, Battement cardiaque (?) & La stimulation est arrêtée \\ \hline
Attentif & Les yeux bougent, il regarde autour de lui, comme en cherchant la source du bruit, il parait concentré & Pendant la stimulation & Caméra & La stimulation continue \\ \hline
Surpris & Il se tend une fraction de seconde, sursaute. & Généralement au début de la stimulation ou après un changement de rythme, de volume, … de la musique & Caméra & La stimulation continue, on observe les comportements suivants. S’il se calme ou il commence à rigoler, nous continuons, s’il reste tendu plus de temps, nous arrêtons. \\ \hline
Tendu & Il ramène les bras vers lui, visage « fermé » & Pendant la stimulation & Caméra & Nous prêtons attention aux prochains comportements, tout en continuant la stimulation \\ \hline
« Change » & Il claque la langue qui est en dehors et en bas, il ne semble pas très convaincu, comme s’il aimerait avoir encore la stimulation, mais pas celle-ci & Toujours avec la même chanson, lorsqu’elle commence il change de comportement par rapport à la chanson suivante. Lorsque nous changeons la chanson, il montre des signes d’appréciation & Caméra & Nous prêtons attention aux prochains comportements, tout en continuant la stimulation \\ \hline
\caption{Synthèse du vocabulaire, avec les signaux et les labels liés}
\label{table:voc}
\end{longtable}

\subsection{Utilisation des labels}
Les labels, comme expliqué dans la section précédente, sont un moyen d'\emph{expliquer} lors de l'entraînement du \ml que deux moments sont similaires, qu'il faut trouver la similitude dans les données à ce moment précis. Par exemple, dans une donnée de type audio, l'utilisateur aura peut-être ajouté deux labels (figure \ref{fig:label_ml}). Les données seront analysées, et le système essayera de trouver une similitude à chaque fois que le label "contentement" sera rencontré. Dans ce cas, on remarque que les courbes se ressemblent quand il y a ces labels. Un algorithme sera calculé. Ensuite, durant la phase autonome, à chaque fois que l'appareil reconnaîtra ce signal sonore, il saura que cela correspond au label "contentement", et exécutera l'action liée à ce label (par exemple, augmenter le volume).

Le terme de \emph{label}, dans notre projet, fera toujours référence à ces données textuels liées à un moment (ponctuel ou avec un début et une fin) et à une autre donnée (vidéo, audio ou autre).

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=\textwidth]{img/schema_label.png}
    \end{center}
    \caption{Fonctionnement de la reconnaissance à partir des labels}
    \label{fig:label_ml}
\end{figure}

\subsection{Capteurs}

Afin de comprendre le vocabulaire de la manière la plus précise possible, un ou plusieurs capteurs seront utilisés dans l'application autonome. 
Nous devons en tester plusieurs afin de trouver lequel ou lesquels sont les plus efficaces.
Ces capteurs doivent permettre de percevoir les signaux transmis par M. F., mais ne doivent pas le déranger, car les données pourraient être faussées. 
Ils doivent également être pratique à utiliser par les accompagnants. 
Nous avons donc sélectionné les capteurs cités ci-après, et avons imaginé comment utiliser leurs données.

\subsubsection{Micro}
Le micro permet d'enregistrer les sons communiqués. Étant donné qu'une grande part de la communication de Monsieur F. passe par les sons, ce capteur est nécessaire. Nous disposons de téléphones Android mis à disposition par l'école, qui permettent justement l'enregistrement de sons. Nous y ajoutons un micro directionnel (également prêté par l'école, branché en jack) afin d'améliorer sensiblement la qualité des enregistrements, et atténuer quelque peu le bruit de l'environnement.

\paragraph{Utilisation}
L'idée serait d'identifier les sons liés au contentement par exemple (claquement de langue). On pourrait alors réagir en accord à chaque fois que le patient fait un claquement de langue.

\subsubsection{Vidéo}
Les vidéos permettent de capter les gestes, et les émotions visibles sur le visage. Le son est également lié aux vidéos. On utilise le capteur photo d'un des smartphones Nexus 5 prêtés. Un support spécifique doit permettre de disposer correctement l'appareil face au sujet. Un bras flexible avec vis de réglage a été commandé (fournisseur: Amazon) et devra être adapté sur le fauteuil de M. F.

\paragraph{Utilisation}
Par reconnaissance faciale, il est peut-être possible de reconnaître la crispation, qui signifie que notre patient est mécontent ou mal à l'aise, et ainsi de réagir en accord.

\subsubsection{Cardiaque}
Nous ne savons pas si les émotions, le contentement ou encore le mécontentement ont un impact sur le rythme cardiaque. Nous allons utiliser une montre connectée Motorola pour capter le rythme cardiaque et vérifier cette hypothèse.

\paragraph{Utilisation}
Si on remarque un rythme cardiaque qui augmente à chaque fois que le label \emph{mécontentement} est utilisé, on aurait une façon simple de reconnaître cette émotion. Le rythme cardiaque a aussi l'avantage de ne pas avoir de \emph{bruit} comme le son, par exemple.

\subsubsection{Bouton}
L'équipe d'accompagnants utilise déjà un bouton pour activer/désactiver des appareils électriques. Malheureusement il n'est pas possible de l'utiliser \emph{programmatiquement} dans une application. Nous allons essayer d'adapter un bouton bluetooth ou jack pour interagir avec le patient. Comme ce dernier en a déjà utilisé un, nous savons d'ores et déjà que cela pourrait fonctionner.

\paragraph{Utilisation}
L'utilisation du bouton est la plus simple. Nous pouvons imiter l'utilisation actuelle faite par les accompagnants. Par exemple, le bouton pouvait être branché au mixer, et M. F. l'enclenchait en appuyant sur le bouton, et restait en fonction tant que le bouton restait enfoncé. M. F. comprend le rapport de cause à effet et apprécie cette activité.\\
Dans le même ordre d'idée, on pourrait activer la musique tant que le patient laisse le bouton enfoncé, et changer de musique s'il le relâche plus d'un certain temps.

\subsubsection{Capteurs non-retenus}
Nous nous sommes limités aux capteurs mis à disposition par l'école ou facilement accessibles et qui ne dérangerait pas le patient. D'autres possibilités ont également été envisagées :

- Capteur cardiaque sur un doigt. Pourrait déranger notre patient.
- Capteur cardiaque sur le torse. Moins pratique que la montre, et certainement plus difficile à interfacer avec un smartphone.
- Kinect. Le système est plus lourd à mettre en place, et nécessite un ordinateur pour enregistrer et traiter les données.

\subsubsection{Matériel}

Voici un résumé du matériel utilisé :

\begin{itemize}
    \item 2 téléphones Android Nexus 5
    \item 1 micro directionnel branché en jack
    \item 1 montre Motorola moto 360 Sport
    \item 1 bouton (à trouver)
    \item 1 support smartphone avec bras flexible et vis de réglage
\end{itemize}


\subsection{Activités}
Le but de notre projet étant d'interagir avec l'environnement de M. F., un certain nombre d'activités doivent être prises en charge. À partir des intérêts de la personne et des possibilités techniques, le choix a été porté sur la musique et la lumière.

\subsubsection{Musique}
La musique est un des passe-temps favoris de M. F. Nous utiliserons donc une enceinte bluetooth, qui est simple à utiliser depuis un smartphone Android et pratique pour les accompagnants. Dans un premier temps, le haut-parleur du smartphone suffira.

\subsubsection{Lumière}
M. F possède déjà un système de lumière Phillips. Malheureusement, il n'est pas possible de l'actionner au travers d'une API. Nous allons donc utiliser une ampoule Phillips Hue afin de permettre à notre patient de \emph{jouer} avec les couleurs de sa chambre.

\subsubsection{Matériel}

Voici un résumé du matériel utilisé :

\begin{itemize}
    \item 1 enceinte bluetooth
    \item 1 ampoule Phillips Hue (y c. Hue bridge / contrôleur)
\end{itemize}