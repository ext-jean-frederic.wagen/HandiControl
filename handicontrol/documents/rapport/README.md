# Rapport HandiControl

Rapport final du projet HandiControl regroupant les informations déterminées par la phase d'analyse, le travail fait lors de l'implémentation, ainsi que des critiques et des pistes pour poursuivre le projet.

## Compilation sous VSCode

- Installer TexLive.
- Installer le plugin *Latex Workshop* pour VSCode.
- Installer Python et Pygments (`pip install Pygments`).
- Ajouter les paramètres de `userSettings.json` aux paramètres de VSCode.