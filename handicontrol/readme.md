# HandiControl

## Demande des clients
Les clients souhaitent un système permettant de comprendre certains besoins et préférences de Monsieur F. lui permettant, peut-être, de pouvoir être autonome lors de certaines activités et d'interagir de manière "indépendante". Les activités mentionnées sont l'écoute de musique et la gestion de l'éclairage.

## HandiControl dataview

Une application multiplateforme de visualisation des données est disponible sur [Gitlab](https://gitlab.com/jdonzallaz/handicontrol-dataview)..


## Release

Pour générer les APKs :

- Copier le fichier `handicontrol-release-key.keystore` dans le dossier *android/app/* du projet.
- Ajouter le mot de passe dans le fichier `android\gradle.properties` du projet. **Le retirer avant de commit/push !**
- Corriger les paramètres des versions supportées dans les `build.gradle` des packages importés (dans les node_modules), selon les paramètres notés ci-dessous.
- Vérifier que les importations de dépendances soient faites avec *implementation* et non *compile* (deprecated).
- Utiliser la commande `gradlew assembleRelease` dans le sous-dossier *android/* du projet.

Note: le fichier .keystore et les mots de passe sont fournis par les mainteneurs du projet.

#### Versions du build.gradle

Tous les projets, sous-projets et dépendances doivent suivre ces versions, pour une release correcte.

```
buildToolsVersion = "28.0.3"
minSdkVersion = 23
compileSdkVersion = 28
targetSdkVersion = 28
supportLibVersion = "28.0.0"

repositories {
    google()    // Verify if google() repo is present
    jcenter()
}
dependencies {
    classpath 'com.android.tools.build:gradle:3.3.0' // Verify if you have the last gradle version
```

## Useful command for development

### debug on watch

```
adb forward tcp:4444 localabstract:/adb-hub
adb connect 127.0.0.1:4444
```

### See Android logs

```
adb -d logcat -s WEARABLE_LISTENER:D WEAR_EVENT_HANDLER:D WEAR_COMMUNICATION:D WEAR_ACTIVITY_STAR
TER:D
adb -e logcat -s START_ACTIVITY_SERVICE:D SMARTWATCH:D
```

### Debugging menu

Open debugging menu on the application.
```
adb shell input keyevent 82
```

### Reload code
Reload the application without relaunching.
```
adb shell input text "RR"
```