/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    View,
    Dimensions,
    NetInfo,
    PermissionsAndroid,
    BackHandler,
    StatusBar,
} from 'react-native';
import { NetworkInfo } from 'react-native-network-info';
import { ToolbarAndroid } from 'react-native-vector-icons/Ionicons';

import HeartRateService from "./services/HeartRateService";
import SocketsService from "./services/SocketsService";
import RemoteService, { Order } from "./services/RemoteService";
import MusicService from "./services/MusicService";
import { statusBarStyle, toolbarStyle } from "./styles/components";
import WearCommunication from './native/WearCommunication';
import VolumeService from "./services/VolumeService";
import StreamConnection from "./components/StreamConnection";


type State = {
    myIP: string,
    portrait: boolean,
    frontCamera: boolean,
}
type Props = {};
export default class App extends Component<Props, State> {

    askPermissionsCount: number = 0;
    streamConnection: StreamConnection;

    constructor() {
        super();

        this.state = {
            portrait: true,
            myIP: '...',
            frontCamera: false,
        };

        MusicService.getInstance(); // Just to load it

        this.handleConnectivityChange = this.handleConnectivityChange.bind(this);
        this.onActionSelected = this.onActionSelected.bind(this);

        HeartRateService.getInstance().addObserver(heartRate => SocketsService.getInstance().sendMessage('heart_rate_update/' + heartRate));
        RemoteService.getInstance().addCallback(Order.PLAY_MUSIC, () => MusicService.getInstance().play());
        RemoteService.getInstance().addCallback(Order.PAUSE_MUSIC, () => MusicService.getInstance().pause());
        RemoteService.getInstance().addCallback(Order.NEXT_MUSIC, () => MusicService.getInstance().next());
        RemoteService.getInstance().addCallback(Order.VOLUME_UP, () => VolumeService.getInstance().volumeUp());
        RemoteService.getInstance().addCallback(Order.VOLUME_DOWN, () => VolumeService.getInstance().volumeDown());
    }

    async componentDidMount() {
        await this.askPermissions();

        MusicService.getInstance().setupPlayer();

        NetInfo.addEventListener('connectionChange', this.handleConnectivityChange);
        NetInfo.getConnectionInfo().then(this.handleConnectivityChange);
    }

    async componentWillUnmount() {
        NetInfo.removeEventListener('connectionChange', this.handleConnectivityChange);
        await MusicService.getInstance().destroy();
    }

    /**
     * Ask multiple permissions at once
     * @returns {Promise}
     */
    askPermissions() {
        return new Promise(resolve => {
            PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            ])
                .then(async (permissionsResult) => {
                    if (Object.values(permissionsResult).some(granted => granted !== PermissionsAndroid.RESULTS.GRANTED)) {
                        console.log("You will not able to use the application without the permissions");
                        if (++this.askPermissionsCount >= 3)
                            BackHandler.exitApp();
                        await this.askPermissions();
                    }
                    resolve();
                })
                .catch(console.error);
        });
    }

    handleConnectivityChange(connectionInfo) {
        if (connectionInfo.type.includes('wifi'))
            NetworkInfo.getIPV4Address(ip => this.setState({ myIP: ip }));
        else
            this.setState({ myIP: 'Pas de connexion Wifi' });
    }

    onLayout(e) {
        const { width, height } = Dimensions.get('window');
        this.setState({ portrait: width < height });
        this.refs.streamConnection.switchOrientation();
    }

    onActionSelected(position) {
        switch (position) {
            case 0: // switch camera
                // this.setState({ frontCamera: !this.state.frontCamera });
                this.refs.streamConnection.switchCamera();
                break;
            case 1: // launch wear app
                WearCommunication.launchWearApp();
                break;
            case 2: // stop music
                MusicService.getInstance().pause();
                break;
        }
    }

    render() {
        return (
            <View onLayout={this.onLayout.bind(this)} style={{ flex: 1 }}>

                <StatusBar backgroundColor={statusBarStyle.backgroundColor}/>

                <ToolbarAndroid
                    title="HandiControl"
                    titleColor="white"
                    actions={toolbarActions}
                    onActionSelected={this.onActionSelected}
                    overflowIconName="md-more"
                    style={toolbarStyle}
                />
                <StreamConnection myIP={this.state.myIP} frontCamera={this.state.frontCamera} ref="streamConnection"/>
            </View>
        );
    }
}

const toolbarActions = [
    { title: 'Changer de caméra', iconName: 'md-reverse-camera', show: 'always' },
    { title: 'Démarrer la montre', iconName: 'md-watch', show: 'always' },
    // { title: 'Arrêter la musique', iconName: 'md-square', show: 'always' },
];
