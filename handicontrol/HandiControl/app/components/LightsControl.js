import React from 'react';
import { Button, View, StyleSheet } from 'react-native';
import LightService, { BRIGHTNESS } from "../services/LightService";
import { buttonColor } from "../styles/components";

export default class LightsControl extends React.Component {
    lightService: LightService;

    constructor() {
        super();

        this.lightService = LightService.getInstance();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.buttonContainer}>
                    <Button onPress={this.lightService.getUsername} title="User" color={buttonColor}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={() => this.lightService.set(BRIGHTNESS.MAX)} title="Max" color={buttonColor}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={() => this.lightService.set(BRIGHTNESS.MIDDLE)} title="Middle"
                            color={buttonColor}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={() => this.lightService.set(BRIGHTNESS.ATTENUATED)} title="Attenuated"
                            color={buttonColor}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={this.lightService.off} title="Off" color={buttonColor}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
        flexDirection: 'row'
    },
    buttonContainer: {
        marginRight: 10,
    },
});
