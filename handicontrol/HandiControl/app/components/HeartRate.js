import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import HeartRateService from "../services/HeartRateService";

export default class HeartRate extends React.Component {
    constructor(props) {
        super(props);

        this.state = { heartRate: 0 };

        // it's a trick! needed in order to overcome the remove event listener
        this.heartRateUpdate = this.heartRateUpdate.bind(this);
    }

    componentDidMount() {
        HeartRateService.getInstance().addObserver(this.heartRateUpdate);
    }

    componentWillUnmount() {
        HeartRateService.getInstance().removeObserver(this.heartRateUpdate);
    }

    heartRateUpdate(heartRate: number) {
        this.setState({ heartRate: heartRate });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.label}>Battements cardiaque :</Text>
                <Text style={styles.heartRate}>{this.state.heartRate}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    label: {
        marginRight: 10,
    },
    heartRate: {
        fontWeight: 'bold',
    },
});
