import React from 'react';
import { Button, View, StyleSheet } from 'react-native';
import LightService, { BRIGHTNESS } from "../services/LightService";

import jsHue from 'jshue';

export default class LightsControl2 extends React.Component {
    lightService;

    constructor() {
        super();
        // this.lightService = LightService.getInstance();

        // var jsHue = require('jsHue')
        this.lightService = jsHue();

        this.lightService.discover().then(bridges => {
            if (bridges.length === 0) {
                console.log('No bridges found. :(');
            } else {
                bridges.forEach(b => console.log('Bridge found at IP address %s.', b.internalipaddress));
            }
        }).catch(e => console.log('Error finding bridges', e));
    }

    render() {
        return (
            <View style={styles.container}>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
        flexDirection: 'row'
    },
    buttonContainer: {
        marginRight: 10,
    }
});
