import { Button, StyleSheet, View } from "react-native";
import React from "react";
import MusicService from "../services/MusicService";
import { buttonStyle } from "../styles/components";
import VolumeService from "../services/VolumeService";

export default class MusicControl extends React.Component {
    musicService: MusicService;
    volumeService: VolumeService;

    constructor() {
        super();
        this.musicService = MusicService.getInstance();
        this.volumeService = VolumeService.getInstance();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.buttonContainer}>
                    <Button onPress={this.musicService.play} title="Play" color={buttonStyle.color}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={this.musicService.pause} title="Pause" color={buttonStyle.color}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={this.musicService.previous} title="Previous" color={buttonStyle.color}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={this.musicService.next} title="Next" color={buttonStyle.color}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={this.volumeService.volumeUp} title="Up" color={buttonStyle.color}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button onPress={this.volumeService.volumeDown} title="Down" color={buttonStyle.color}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
        flexDirection: 'row'
    },
    buttonContainer: {
        marginRight: 10,
    },
});
