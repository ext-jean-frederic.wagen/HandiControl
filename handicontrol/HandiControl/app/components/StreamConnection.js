/**
 * StreamConnection component containing the network Informations
 */
import React, { Component } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import store from 'react-native-simple-store';
import HeartRate from "./HeartRate";
import CameraView from "../native/CameraView";
import SocketsService from "../services/SocketsService";
import MusicService from "../services/MusicService";
import RemoteService, { DataPath, Order } from "../services/RemoteService";


const STREAM_URL: string = 'rtsp://127.0.0.1:5540/live/stream';
const RECORD_PATH: string = '/HandiControl-Videos';
const CAMERA_BACK: string = 'BACK';
const CAMERA_FRONT: string = 'FRONT';

/**
 * TODO: ratio preview
 * TODO: orientation finish and fix + clean
 */

type State = {
    start: boolean,
    videoEnabled: boolean,
}

type Props = {
    myIP: string,
    frontCamera: boolean,
};

export default class StreamConnection extends Component<Props, State> {

    cameraView: CameraView;
    socketService: SocketsService;
    cameraFace: string = CAMERA_BACK;

    constructor(props: Props) {
        super(props);

        this.state = {
            start: false,
            videoEnabled: true,
        };

        this.socketService = SocketsService.getInstance();

        this.onStartButton = this.onStartButton.bind(this);
        this.updateCameraFace = this.updateCameraFace.bind(this);
        this.switchCamera = this.switchCamera.bind(this);
        this.blackImage = this.blackImage.bind(this);
        this.stopStreaming = this.stopStreaming.bind(this);
        RemoteService.getInstance().addCallback(Order.START_RECORDING, (path, data) =>{
            this.cameraView.startStream(STREAM_URL);
            this.cameraView.startRecord(RECORD_PATH, '' + data);
        });
        RemoteService.getInstance().addCallback(Order.STOP_RECORDING, () => {
            this.cameraView.stopRecord();
            this.cameraView.stopStream();
        });
        RemoteService.getInstance().addCallback(Order.BLACK_IMAGE, this.blackImage);
    }

    switchOrientation() {
        //this.cameraView.fixOrientation();
    }

    updateCameraFace() {
        store.get('cameraFace').then(cameraFace => {
            if (cameraFace) {
                this.cameraFace = cameraFace;
                this.cameraView.setCameraFace(cameraFace);
            }
        });
    }

    switchCamera() {
        this.cameraView.switchCamera();
        this.cameraFace = this.cameraFace === CAMERA_FRONT ? CAMERA_BACK : CAMERA_FRONT;
        store.save('cameraFace', this.cameraFace);
    }

    async onStartButton() {
        await this.setState({ start: !this.state.start });
        if (this.state.start) {
            // this.cameraView.startStream(STREAM_URL);
            // this.cameraView.startRecord(RECORD_PATH);
            this.socketService.startServer(() => {
            });
        } else {
            this.stopStreaming();
        }
    }

    stopStreaming() {
        this.cameraView.stopRecord();
        this.cameraView.stopStream();
        this.socketService.closeSockets();
        MusicService.getInstance().pause();
    }

    componentDidMount() {
        this.updateCameraFace();
    }

    componentWillUnmount() {
        this.stopStreaming();
    }

    blackImage() {
        this.cameraView.disableVideo();
        RemoteService.getInstance().send(DataPath.BLACK_IMAGE_TIME, Date.now());
        setTimeout(() => this.cameraView.enableVideo(), 1500);
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.container}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>IP : {this.props.myIP}</Text>
                        <View style={{ marginLeft: 'auto' }}>
                            <HeartRate/>
                        </View>
                    </View>
                    {/*<WearAppLauncher/>*/}
                    {/*<LightsControl/>*/}
                    {/*<MusicControl/>*/}
                    <CameraView style={{ flex: 1, marginBottom: 10 }} ref={cameraView => this.cameraView = cameraView}/>
                    <Button onPress={this.onStartButton} title={this.state.start ? "Arrêter" : "Démarrer"}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
});