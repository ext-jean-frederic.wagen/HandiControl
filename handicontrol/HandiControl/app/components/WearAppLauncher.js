import React from 'react';
import { Button, View, StyleSheet } from 'react-native';
import WearCommunication from './../native/WearCommunication';
import { buttonColor } from "../styles/components";

export default class WearAppLauncher extends React.Component {
    launchWearApp() {
        WearCommunication.launchWearApp();
    }

    render() {
        return (
            <View style={styles.container}>
                <Button onPress={this.launchWearApp} title="Démarrer la montre" color={buttonColor}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
    },
});
