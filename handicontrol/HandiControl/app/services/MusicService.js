import TrackPlayer from 'react-native-track-player';

const RNFS = require('react-native-fs');
import MediaMeta from 'react-native-media-meta';

export default class MusicService {
    static instance: MusicService;

    static getInstance(): MusicService {
        if (MusicService.instance == null)
            MusicService.instance = new MusicService();

        return this.instance;
    }

    hasMusic: boolean = false;

    constructor() {
        this.play = this.play.bind(this);
        this.pause = this.pause.bind(this);
        this.stop = this.stop.bind(this);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.destroy = this.destroy.bind(this);
    }

    setupPlayer() {
        TrackPlayer.setupPlayer({}).then(async () =>
            RNFS.readDir('/storage/emulated/0/Music/')
                .then(tracks => {
                    if (tracks.length > 0) {
                        this.hasMusic = true;
                        console.log("Music: found " + tracks.length + " tracks.");
                    } else
                        console.log("Music: no track found.");

                    for (const track of tracks) {
                        MediaMeta.get(track.path)
                            .then(metadata => TrackPlayer.add([{
                                id: (metadata.track + metadata.title + metadata.artist + metadata.album).split(' ').join(''),
                                url: 'file://' + track.path, // Load media from the app bundle
                                title: metadata.title,
                                artist: metadata.artist,
                            }]))
                            .catch(console.error);
                    }
                }).catch(() => {
                console.log("Music: folder not found.");
            }) // Folder does not exist
        );

        TrackPlayer.updateOptions({
            // An array of media controls capabilities, can contain CAPABILITY_PLAY, CAPABILITY_PAUSE, CAPABILITY_STOP,
            // CAPABILITY_SEEK_TO, CAPABILITY_SKIP_TO_NEXT, CAPABILITY_SKIP_TO_PREVIOUS, CAPABILITY_SET_RATING
            /*capabilities: [],*/

            stopWithApp: true,

            // Notification Color (Must be an ARGB Hexadecimal number)
            color: 0x6f42c1
        });
    }

    async destroy() {
        await TrackPlayer.reset();
        TrackPlayer.destroy();
    }

    play() {
        if (this.hasMusic)
            TrackPlayer.play();
    }

    pause() {
        TrackPlayer.pause();
    }

    stop() {
        TrackPlayer.stop();
    }

    next() {
        if (this.hasMusic)
            TrackPlayer.getQueue().then(tracks => {
                if (tracks.length)
                    TrackPlayer.getCurrentTrack().then(id => {
                        if (tracks.map(track => track.id).indexOf(id) < tracks.length - 1)
                            TrackPlayer.skipToNext();
                        else
                            TrackPlayer.skip(tracks[0].id);
                    });
            });
    }

    previous() {
        if (this.hasMusic)
            TrackPlayer.getQueue().then(tracks => {
                if (tracks.length)
                    TrackPlayer.getCurrentTrack().then(id => {
                        if (tracks.map(track => track.id).indexOf(id) > 0)
                            TrackPlayer.skipToPrevious();
                        else
                            TrackPlayer.skip(tracks[0].id);
                    });
            });
    }

    static playerHandler = async (data) => {
        if (data.type === 'playback-state') {
            // Update the UI with the new state
        } else if (data.type === 'remote-play') {
            // The play button was pressed, we can forward this command to the player using
            TrackPlayer.play();
        } else if (data.type === 'remote-stop') {
            // The stop button was pressed, we can stop the player
            TrackPlayer.stop();
        } else if (data.type === 'remote-pause') {
            // The play button was pressed, we can forward this command to the player using
            TrackPlayer.pause();
        } else if (data.type === 'playback-queue-ended') {
            console.log("END");
            TrackPlayer.getQueue().then(tracks => TrackPlayer.skip(tracks[0].id));
        }
    };
}
