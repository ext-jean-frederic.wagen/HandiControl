import SystemSetting from 'react-native-system-setting';

const VOLUME_INCREMENT: number = 0.08;

export default class VolumeService {
    static instance: VolumeService;

    static getInstance(): VolumeService {
        if (VolumeService.instance == null)
            VolumeService.instance = new VolumeService();

        return this.instance;
    }

    constructor() {
        this.volumeUp = this.volumeUp.bind(this);
        this.volumeDown = this.volumeDown.bind(this);
        this.setVolume = this.setVolume.bind(this);
    }

    // type must be one of music, call, system, ring, alarm, notification, default is music
    async volumeUp(type: string = 'music') {
        const volume: number = await SystemSetting.getVolume();
        console.log('volume', volume);
        this.setVolume(volume + VOLUME_INCREMENT);
    }

    // type must be one of music, call, system, ring, alarm, notification, default is music
    async volumeDown(type: string = 'music') {
        const volume: number = await SystemSetting.getVolume();
        console.log('volume', volume);
        this.setVolume(volume - VOLUME_INCREMENT);
    }

    /*
     * volume must be between 0 and 1
     * type must be one of music, call, system, ring, alarm, notification, default is music
     */
    setVolume(volume: number, type: string = 'music') {
        if (volume > 1) volume = 1;
        else if (volume < 0) volume = 0;
        SystemSetting.setVolume(volume, { type });
    }
}