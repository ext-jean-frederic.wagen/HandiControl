import { DeviceEventEmitter } from 'react-native';

const HEART_RATE_EVENT_NAME: string = 'heart_rate_update';

export default class HeartRateService {
    observersFunctions: Set<Function> = new Set();

    static instance: HeartRateService;

    static getInstance(): HeartRateService {
        if (HeartRateService.instance == null)
            HeartRateService.instance = new HeartRateService();

        return this.instance;
    }

    constructor() {
        // it's a trick! needed in order to overcome the remove event listener
        this.notifyObservers = this.notifyObservers.bind(this);
    }

    notifyObservers(event) {
        for (let observer of this.observersFunctions)
            observer(event);
    }

    addObserver(observer: Function) {
        if (this.observersFunctions.size === 0)
            DeviceEventEmitter.addListener(HEART_RATE_EVENT_NAME, this.notifyObservers);

        this.observersFunctions.add(observer);
    }

    removeObserver(observer: Function) {
        this.observersFunctions.delete(observer);

        if (this.observersFunctions.size === 0)
            DeviceEventEmitter.removeListener(HEART_RATE_EVENT_NAME, this.notifyObservers);
    }
}