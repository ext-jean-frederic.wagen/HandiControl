import { ToastAndroid, AsyncStorage } from "react-native";

export const BRIGHTNESS = {
    MAX: 254,
    MIDDLE: 127,
    ATTENUATED: 63
};

export default class LightService {
    static instance: LightService;

    username: string;
    ipAddress: string = '192.168.1.49';//192.168.1.47';
    apiEndpoint: string = 'api';

    static getInstance(): LightService {
        if (LightService.instance == null)
            LightService.instance = new LightService();

        return this.instance;
    }

    constructor() {
        this.getUsername = this.getUsername.bind(this);
        this.set = this.set.bind(this);
        this.off = this.off.bind(this);
    }

    a() {
        fetch('http://' + this.ipAddress + '/' + this.apiEndpoint + '/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                devicetype: 'HandiControl'
            }),
        })
            .then((response) => response.json())
            .then(async response => {
                console.log(response);
                if (response[0] && response[0].success && response[0].success.username) {
                    // this.username = response[0].success.username;
                    await AsyncStorage.setItem('lightUsername', response[0].success.username);
                    this.username = response[0].success.username;
                    ToastAndroid.show('Connecté au pont Philips Hue', ToastAndroid.LONG);
                } else
                    ToastAndroid.show('Erreur: le bouton de lien du pont Philips Hue doit être pressé', ToastAndroid.LONG);
            })
    }

    async getUsername() {
        // this.username = await AsyncStorage.getItem('lightUsername') || null;

        if (!this.username)
            this.a();
        else
            ToastAndroid.show('Connecté au pont Philips Hue', ToastAndroid.LONG);
    }

    set(brightness: number) {
        this.update(brightness);
    }

    off() {
        this.update(0);
    }

    update(brightness: number) {
        let on = brightness > 0;

        fetch('http://' + this.ipAddress + '/' + this.apiEndpoint + '/' + this.username + '/lights/2/state', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                on: on,
                bri: brightness
            }),
        })
            .then(response => console.log(response))
            .catch(reason => console.error(reason));
    }
}