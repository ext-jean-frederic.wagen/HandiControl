import SocketsService from "./SocketsService";

export default class RemoteService {
    static instance: RemoteService;

    static getInstance(): RemoteService {
        if (RemoteService.instance == null)
            RemoteService.instance = new RemoteService();

        return this.instance;
    }

    socketsService: SocketsService;
    callbacks: Map = new Map();

    constructor() {
        this.messageCallback = this.messageCallback.bind(this);

        this.socketsService = SocketsService.getInstance();
        this.socketsService.addObserver(this.messageCallback);
    }

    messageCallback(message: string) {
        const pathSeparatorIndex: number = message.indexOf(pathSeparator);
        const path: string = pathSeparatorIndex > -1 ? message.split(pathSeparator)[0] : message;
        const data: string = pathSeparatorIndex > -1 ? message.substring(pathSeparatorIndex + 1) : '';

        console.log('path: ' + path, 'data: ' + data);

        const callback: Function = this.callbacks.get(path);

        if (callback) callback(path, data);
    }

    addCallback(path: string, callback: Function) {
        this.callbacks.set(path, callback);
    }

    removeCallback(path: string) {
        this.callbacks.delete(path);
    }

    sendOrder(order: string) {
        this.socketsService.sendMessage(order);
    }

    send(path: string, data: string = false) {
        if (data !== false)
            this.socketsService.sendMessage(path + '/' + data);
        else
            this.socketsService.sendMessage(path);
    }
}

const pathSeparator: string = '/';

const Order = {
    START_RECORDING: 'START_RECORDING',
    STOP_RECORDING: 'STOP_RECORDING',
    PLAY_MUSIC: 'PLAY_MUSIC',
    PAUSE_MUSIC: 'PAUSE_MUSIC',
    NEXT_MUSIC: 'NEXT_MUSIC',
    VOLUME_UP: 'VOLUME_UP',
    VOLUME_DOWN: 'VOLUME_DOWN',
    BLACK_IMAGE: 'BLACK_IMAGE',
};

const DataPath = {
    HEART_RATE_UPDATE_PATH: 'heart_rate_update',
    BLACK_IMAGE_TIME: 'BLACK_IMAGE_TIME',
};

export { Order, DataPath };