import Sockets from 'react-native-sockets';
import { DeviceEventEmitter } from 'react-native';
import { ToastAndroid } from 'react-native';

export default class SocketsService {
    static instance: SocketsService;

    static getInstance(): SocketsService {
        if (SocketsService.instance == null)
            SocketsService.instance = new SocketsService();

        return this.instance;
    }

    callBackOnMessage;
    callBackOnConnected;
    callBackOnError;
    server = false;
    port = 9321;
    clientConnected;
    isConnected = false;

    constructor() {
        this.callBackOnMessage = null;
        this.callBackOnConnected = null;
        this.callBackOnError = null;
        this.server = false;
        this.clientConnected = null;
        this.isConnected = false;

        this.onSocketClient_connected = this.onSocketClient_connected.bind(this);
        this.onSocketClient_data = this.onSocketClient_data.bind(this);
        this.onSocketClient_closed = this.onSocketClient_closed.bind(this);
        this.onSocketClient_error = this.onSocketClient_error.bind(this);

        this.onSocketServer_connected = this.onSocketServer_connected.bind(this);
        this.onSocketServer_data = this.onSocketServer_data.bind(this);
        this.onSocketServer_clientConnected = this.onSocketServer_clientConnected.bind(this);
        this.onSocketServer_clientDisconnected = this.onSocketServer_clientDisconnected.bind(this);
        this.onSocketServer_closed = this.onSocketServer_closed.bind(this);
        this.onSocketServer_error = this.onSocketServer_error.bind(this);
    }

    sendMessage(message) {
        if (!this.isConnected) return;
        if (this.server)
            Sockets.emit(message, this.clientConnected);
        else
            Sockets.write(message);
    }

    startServer(callBackOnMessage) {
        this.callBackOnMessage = callBackOnMessage;
        this.server = true;
        this.addServerListeners();
        Sockets.startServer(this.port);
    }

    startClient(address, callBackOnConnected, callBackOnError, callBackOnMessage) {
        this.callBackOnConnected = callBackOnConnected;
        this.callBackOnError = callBackOnError;
        this.callBackOnMessage = callBackOnMessage;
        this.server = false;
        let config = {
            address: "" + address,
            port: this.port,
        };
        this.addClientListeners();
        Sockets.startClient(config);
    }

    closeSockets() {
        console.log("CloseSockets")
        if (this.server) {
            Sockets.close();
            this.removeServerListeners();
        } else {
            Sockets.disconnect();
            this.removeClientListeners();
        }
    }

    // ----- Add / remove listeners

    addServerListeners() {
        DeviceEventEmitter.addListener('socketServer_connected', this.onSocketServer_connected);
        DeviceEventEmitter.addListener('socketServer_error', this.onSocketServer_error);
        DeviceEventEmitter.addListener('socketServer_clientConnected', this.onSocketServer_clientConnected);
        DeviceEventEmitter.addListener('socketServer_data', this.onSocketServer_data);
        DeviceEventEmitter.addListener('socketServer_closed', this.onSocketServer_closed);
        DeviceEventEmitter.addListener('socketServer_clientDisconnected', this.onSocketServer_clientDisconnected);
    }

    removeServerListeners() {
        DeviceEventEmitter.removeListener('socketServer_connected', this.onSocketServer_connected);
        DeviceEventEmitter.removeListener('socketServer_error', this.onSocketServer_error);
        DeviceEventEmitter.removeListener('socketServer_clientConnected', this.onSocketServer_clientConnected);
        DeviceEventEmitter.removeListener('socketServer_data', this.onSocketServer_data);
        DeviceEventEmitter.removeListener('socketServer_closed', this.onSocketServer_closed);
        DeviceEventEmitter.removeListener('socketServer_clientDisconnected', this.onSocketServer_clientDisconnected);
    }

    addClientListeners() {
        DeviceEventEmitter.addListener('socketClient_connected', this.onSocketClient_connected);
        DeviceEventEmitter.addListener('socketClient_error', this.onSocketClient_error);
        DeviceEventEmitter.addListener('socketClient_data', this.onSocketClient_data);
        DeviceEventEmitter.addListener('socketClient_closed', this.onSocketClient_closed);
    }

    removeClientListeners() {
        DeviceEventEmitter.removeListener('socketClient_connected', this.onSocketClient_connected);
        DeviceEventEmitter.removeListener('socketClient_error', this.onSocketClient_error);
        DeviceEventEmitter.removeListener('socketClient_data', this.onSocketClient_data);
        DeviceEventEmitter.removeListener('socketClient_closed', this.onSocketClient_closed);
    }

    // ----- Observers

    observersFunctions: Set<Function> = new Set();

    notifyObservers(data: string) {
        for (const observer of this.observersFunctions)
            observer(data);
    }

    addObserver(observer: Function) {
        this.observersFunctions.add(observer);
    }

    removeObserver(observer: Function) {
        this.observersFunctions.delete(observer);
    }

    // ----- Events handler client

    onSocketClient_connected() {
        console.log('socketClient_connected');
        this.isConnected = true;
        if (this.callBackOnConnected)
            this.callBackOnConnected();
    }

    onSocketClient_error(data) {
        console.log('socketClient_error', data.error);
        if (this.callBackOnError)
            this.callBackOnError();
    }

    onSocketClient_data(payload) {
        console.log('Server -> Client', payload.data);
        if (this.callBackOnMessage)
            this.callBackOnMessage(payload.data);
        this.notifyObservers(payload.data);
    }

    onSocketClient_closed(data) {
        console.log('socketClient_closed', data.error);
        this.isConnected = false;
    }

    // ----- Events handler server

    onSocketServer_connected() {
        console.log('socketServer_connected');
    }

    onSocketServer_error(data) {
        console.log('socketServer_error', data.error);
    }

    onSocketServer_clientConnected(client) {
        console.log('socketServer_clientConnected', client.id);
        this.clientConnected = client.id;
        this.isConnected = true;
    }

    onSocketServer_data(payload) {
        console.log('Client -> Server : ', payload.data);
        if (this.callBackOnMessage)
            this.callBackOnMessage(payload.data);
        this.notifyObservers(payload.data);
    }

    onSocketServer_closed(data) {
        console.log('socketServer_closed', data.error);
        this.isConnected = false;
        this.clientConnected = null;
    }

    onSocketServer_clientDisconnected(data) {
        console.log('socketServer_clientDisconnected client id:', data.client);
        this.isConnected = false;
        this.clientConnected = null;
    }
}
