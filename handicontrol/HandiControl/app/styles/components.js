import { primaryColor, primaryDarkColor } from "./colors";

const buttonColor: string = primaryColor;

const buttonStyle = {
    color: buttonColor,
};


const toolbarStyle = {
    backgroundColor: primaryColor,
    height: 56,
    elevation: 4, // 4dp
};

const statusBarStyle = {
    backgroundColor: primaryDarkColor,
};

export { buttonColor, buttonStyle, toolbarStyle, statusBarStyle };