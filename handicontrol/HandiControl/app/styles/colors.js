const primaryColor: string = '#673ab7'; // deep purple 500
const primaryLightColor: string = '#9a67ea'; // light
const primaryDarkColor: string = '#320b86'; // dark

const secondaryColor: string = '#fdd835'; // yellow 600
const secondaryLightColor: string = '#ffff6b'; // light
const secondaryDarkColor: string = '#c6a700'; // dark

export {
    primaryColor,
    primaryLightColor,
    primaryDarkColor,
    secondaryColor,
    secondaryLightColor,
    secondaryDarkColor,
};