import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { requireNativeComponent, View, UIManager, findNodeHandle } from 'react-native';

const CAMERA_VIEW_REF = 'CameraView';
const RCTCameraView = requireNativeComponent(CAMERA_VIEW_REF, CameraView);

// export const CAMERA_BACK: string = 'BACK'; //UIManager.CameraView.Constants.CAMERA_BACK;
// export const CAMERA_FRONT: string = 'FRONT'; //UIManager.CameraView.Constants.CAMERA_FRONT;

class CameraView extends Component {
    switchCamera() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.switchCamera,
            null
        );
    }

    setCameraFace(cameraFace: string) {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.setCameraFace,
            [cameraFace]
        );
    }

    fixOrientation() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.fixOrientation,
            []
        );
    }

    enableVideo() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.enableVideo,
            []
        );
    }

    disableVideo() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.disableVideo,
            []
        );
    }

    startRecord(recordPath: string, recordName: string) {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.startRecord,
            [recordPath, recordName]
        );
    }

    stopRecord() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.stopRecord,
            null
        );
    }

    startStream(streamUrl: string) {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.startStream,
            [streamUrl]
        );
    }

    stopStream() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs[CAMERA_VIEW_REF]),
            UIManager.CameraView.Commands.stopStream,
            null
        );
    }

    render() {
        return <RCTCameraView ref={CAMERA_VIEW_REF} {...this.props} />;
    };
}

CameraView.name = CAMERA_VIEW_REF;
CameraView.propTypes = {
    cameraFace: PropTypes.bool,
    ...View.propTypes
};

module.exports = CameraView;
