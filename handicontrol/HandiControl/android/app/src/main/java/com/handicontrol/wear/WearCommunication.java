package com.handicontrol.wear;

import android.net.Uri;

import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.Wearable;
import com.handicontrol.R;

public class WearCommunication extends ReactContextBaseJavaModule implements LifecycleEventListener {
    private static final String TAG = "WEAR_COMMUNICATION";

    private final WearEventHandler mEventHandler;
    private final WearActivityStarter mWearActivityStarter;
    private final Uri heartRateUpdateMessageFilter;

    public WearCommunication(ReactApplicationContext reactContext) {
        super(reactContext);

        heartRateUpdateMessageFilter = Uri.parse(reactContext.getString(R.string.message_base_path) +
                reactContext.getString(R.string.heart_rate_update_path));

        mEventHandler = new WearEventHandler(reactContext);
        Wearable.getMessageClient(reactContext).addListener(
                mEventHandler,
                heartRateUpdateMessageFilter,
                MessageClient.FILTER_PREFIX);

        mWearActivityStarter = new WearActivityStarter(reactContext);
    }

    @Override
    public String getName() {
        return "WearCommunication";
    }

    @ReactMethod
    public void launchWearApp() {
        mWearActivityStarter.startActivity();
    }

    @Override
    public void onHostResume() {
        Wearable.getMessageClient(getReactApplicationContext()).addListener(
                mEventHandler,
                heartRateUpdateMessageFilter,
                MessageClient.FILTER_PREFIX);
    }

    @Override
    public void onHostPause() {
        Wearable.getMessageClient(getReactApplicationContext()).removeListener(mEventHandler);
    }

    @Override
    public void onHostDestroy() {
        Wearable.getMessageClient(getReactApplicationContext()).addListener(mEventHandler);
    }
}