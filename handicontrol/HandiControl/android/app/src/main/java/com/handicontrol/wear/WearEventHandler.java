package com.handicontrol.wear;

import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.handicontrol.R;

import java.nio.charset.Charset;

public class WearEventHandler implements MessageClient.OnMessageReceivedListener {
    private static final String TAG = "WEAR_EVENT_HANDLER";

    private ReactApplicationContext mReactContext;

    public WearEventHandler(ReactApplicationContext context) {
        mReactContext = context;
    }

    @Override
    public void onMessageReceived(@NonNull MessageEvent messageEvent) {
        byte[] data = messageEvent.getData();
        String message = new String(data, Charset.forName("UTF-8"));
        String path = messageEvent.getPath();

        Log.d(TAG, "Heart rate update in-app: " + path + " / " + message);

        mReactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(mReactContext.getString(R.string.heart_rate_update_event), message);
    }
}