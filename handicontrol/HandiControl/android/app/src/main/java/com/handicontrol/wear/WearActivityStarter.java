package com.handicontrol.wear;

import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.handicontrol.R;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class WearActivityStarter {
    private static final String TAG = "WEAR_ACTIVITY_STARTER";

    private ReactApplicationContext mReactContext;

    public WearActivityStarter(ReactApplicationContext context) {
        mReactContext = context;
    }

    public void startActivity() {
        new WearActivityStarter.SendThread(mReactContext.getString(R.string.start_activity_path), "").start();
    }

    //This actually sends the message to the wearable device.
    class SendThread extends Thread {
        String path;
        String message;

        //constructor
        SendThread(String path, String message) {
            this.path = path;
            this.message = message;
        }

        //sends the message via the thread.  this will send to all wearables connected, but
        //since there is (should only?) be one, so no problem.
        public void run() {
            //first get all the nodes, ie connected wearable devices.
            Task<List<Node>> nodeListTask =
                    Wearable.getNodeClient(mReactContext).getConnectedNodes();
            try {
                // Block on a task and get the result synchronously (because this is on a background
                // thread).
                List<Node> nodes = Tasks.await(nodeListTask);

                //Now send the message to each device.
                for (Node node : nodes) {
                    Task<Integer> sendMessageTask = Wearable.getMessageClient(mReactContext)
                            .sendMessage(node.getId(), path, message.getBytes());

                    try {
                        // Block on a task and get the result synchronously (because this is on a background
                        // thread).
                        Integer result = Tasks.await(sendMessageTask);
                        Log.d(TAG, "SendThread: message send to " + node.getDisplayName() + " : " + message);

                    } catch (ExecutionException exception) {
                        Log.e(TAG, "Task failed: " + exception);

                    } catch (InterruptedException exception) {
                        Log.e(TAG, "Interrupt occurred: " + exception);
                    }
                }

            } catch (ExecutionException exception) {
                Log.e(TAG, "Task failed: " + exception);

            } catch (InterruptedException exception) {
                Log.e(TAG, "Interrupt occurred: " + exception);
            }
        }
    }
}
