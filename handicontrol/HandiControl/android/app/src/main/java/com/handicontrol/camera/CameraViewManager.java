package com.handicontrol.camera;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.pedro.encoder.input.video.CameraHelper;

import java.util.Map;

import javax.annotation.Nullable;

public class CameraViewManager extends SimpleViewManager<CameraView> {
    private static final int COMMAND_START_STREAM = 0;
    private static final int COMMAND_STOP_STREAM = 1;
    private static final int COMMAND_START_RECORD = 2;
    private static final int COMMAND_STOP_RECORD = 3;
    private static final int COMMAND_SWITCH_CAMERA = 4;
    private static final int COMMAND_SET_CAMERA_FACE = 5;
    private static final int COMMAND_FIX_ORIENTATION = 6;
    private static final int COMMAND_ENABLE_VIDEO = 7;
    private static final int COMMAND_DISABLE_VIDEO = 8;

    @Override
    public String getName() {
        return "CameraView";
    }

    @Override
    protected CameraView createViewInstance(ThemedReactContext reactContext) {
        return new CameraView(reactContext);
    }

    @Nullable
    @Override
    public Map<String, Object> getExportedViewConstants() {
        return MapBuilder.of(
                "CAMERA_BACK", CameraHelper.Facing.BACK.name(),
                "CAMERA_FRONT", CameraHelper.Facing.FRONT.name()
        );
    }

    @Nullable
    @Override
    public Map<String, Integer> getCommandsMap() {
        MapBuilder.Builder<String, Integer> builder = MapBuilder.builder();
        return builder
                .put("startStream", COMMAND_START_STREAM)
                .put("stopStream", COMMAND_STOP_STREAM)
                .put("startRecord", COMMAND_START_RECORD)
                .put("stopRecord", COMMAND_STOP_RECORD)
                .put("switchCamera", COMMAND_SWITCH_CAMERA)
                .put("setCameraFace", COMMAND_SET_CAMERA_FACE)
                .put("fixOrientation", COMMAND_FIX_ORIENTATION)
                .put("enableVideo", COMMAND_ENABLE_VIDEO)
                .put("disableVideo", COMMAND_DISABLE_VIDEO)
                .build();
    }

    @Override
    public void receiveCommand(CameraView cameraView, int commandId, @Nullable ReadableArray args) {
        switch (commandId) {
            case COMMAND_START_STREAM:
                cameraView.startStream(args.getString(0));
                break;
            case COMMAND_STOP_STREAM:
                cameraView.stopStream();
                break;
            case COMMAND_START_RECORD:
                cameraView.startRecord(args.getString(0), args.getString(1));
                break;
            case COMMAND_STOP_RECORD:
                cameraView.stopRecord();
                break;
            case COMMAND_SWITCH_CAMERA:
                cameraView.switchCamera();
                break;
            case COMMAND_SET_CAMERA_FACE:
                cameraView.setCameraFace(CameraHelper.Facing.valueOf(args.getString(0)));
                break;
            case COMMAND_FIX_ORIENTATION:
                cameraView.fixOrientation();
                break;
            case COMMAND_ENABLE_VIDEO:
                cameraView.enableVideo();
                break;
            case COMMAND_DISABLE_VIDEO:
                cameraView.disableVideo();
                break;
        }
    }
}
