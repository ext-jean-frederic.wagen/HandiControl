package com.handicontrol.camera;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.RelativeLayout;

import com.pedro.encoder.input.video.CameraHelper;
import com.pedro.rtplibrary.network.AdapterBitrateParser;
import com.pedro.rtplibrary.network.ConnectionClassManager;
import com.pedro.rtplibrary.network.UploadBandwidthSampler;
import com.pedro.rtplibrary.rtsp.RtspCamera1;
import com.pedro.rtplibrary.view.OpenGlView;
import com.pedro.rtsp.rtsp.Protocol;
import com.pedro.rtsp.utils.ConnectCheckerRtsp;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

class CameraView extends RelativeLayout implements SurfaceHolder.Callback {
    private static final String TAG = "CameraView";
    private int attachCount = 0;
    private ConnectCheckerRtsp connectCheckerRtsp = new Checker();
    private SurfaceView surfaceView;
    private RtspCamera1 rtspCamera;
    private CameraHelper.Facing cameraFace = CameraHelper.Facing.BACK;
    private ConnectionClassManager connectionClassManager;
    private UploadBandwidthSampler uploadBandwidthSampler;
    private Camera.Size cameraSize;

    public CameraView(Context context) {
        super(context, null);

        surfaceView = new OpenGlView(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d(TAG, "onAttachedToWindow");

        if (attachCount++ != 0) return;
        Log.d(TAG, "onStart");

        addView(surfaceView);
        surfaceView.getHolder().addCallback(this);

        connectionClassManager = ConnectionClassManager.getInstance();
        uploadBandwidthSampler = UploadBandwidthSampler.getInstance();

        rtspCamera = new RtspCamera1(surfaceView, connectCheckerRtsp);
        rtspCamera.setProtocol(Protocol.TCP);

        computeResolution();
        addAdaptiveBitrate();
        prepareInputs();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.d(TAG, "onDetachedFromWindow");
        connectionClassManager.remove();
    }

    private void prepareInputs() {
        Log.d(TAG, "preparing inputs");

        if (rtspCamera.prepareAudio() && rtspCamera.prepareVideo(
                cameraSize.width, cameraSize.height,
                15,
                700 * 1024,
                false,
                getCameraOrientation()
        )) {
            Log.d(TAG, "Finish preparing inputs");
        } else {
            /*
             * This device cant init encoders, this could be for 2 reasons: The encoder selected doesnt
             * support any configuration setted or your device hasnt a H264 or AAC encoder (in this case you can see log error valid encoder not found)
             */
            Log.e(TAG, "Error preparing inputs");
        }
    }

    private void addAdaptiveBitrate() {
        //Let library auto calculate your max video bitrate related by your resolution.
        AdapterBitrateParser.calculateMaxVideoBitrate(cameraSize.width * cameraSize.height);

        //Select your own max video bitrate (kbps)
        // AdapterBitrateParser.maxVideoBitrate = 1200;

        //Set delay to check video bitrate (ms)
        AdapterBitrateParser.DELAY = 1000;

        //Set video bitrate interval that library will use to increase or decrease it (kbps).
        AdapterBitrateParser.DIFFERENCE = 250;

        connectionClassManager.register(
                bandwidth -> AdapterBitrateParser.parseBitrate(
                        rtspCamera.getBitrate(),
                        (int) bandwidth,
                        bitrate -> rtspCamera.setVideoBitrateOnFly(bitrate)
                )
        );
    }

    private void computeResolution() {
        List<Camera.Size> sizes = rtspCamera.getResolutionsBack();
        Collections.sort(sizes, (size1, size2) -> size1.width - size2.width);
        cameraSize = sizes.get(0);
        for (Camera.Size size : sizes) {
            if (size.width > 640) break;
            cameraSize = size;
        }

        Log.d(TAG, "Set resolution: " + cameraSize.width + "x" + cameraSize.height);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // Log.d(TAG, "SurfaceCreated");
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        // Log.d(TAG, "surfaceChanged");
        startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Log.d(TAG, "surfaceDestroyed");
        stopStream();
        stopRecord();
        stopPreview();
    }

    public void fixOrientation() {
        if (rtspCamera != null)
            rtspCamera.setPreviewOrientation(getCameraOrientation());
    }

    private int getCameraOrientation() {
        return CameraHelper.getCameraOrientation(getContext());
    }

    public void setCameraFace(CameraHelper.Facing cameraFace) {
        Log.d(TAG, "Set camera face: " + cameraFace.name());
        this.cameraFace = cameraFace;
        if (rtspCamera != null && (rtspCamera.isFrontCamera() ^ cameraFace.equals(CameraHelper.Facing.FRONT)))
            rtspCamera.switchCamera();
    }

    public void startPreview() {
        Log.d(TAG, "starting preview");
        rtspCamera.startPreview(cameraFace);
    }

    public void stopPreview() {
        Log.d(TAG, "stopping preview");
        rtspCamera.stopPreview();
    }

    public void startStream(String streamUrl) {
        if (rtspCamera != null && !rtspCamera.isStreaming()) {
            uploadBandwidthSampler.startSampling();
            rtspCamera.startStream(streamUrl);
        }
    }

    public void stopStream() {
        if (rtspCamera != null && rtspCamera.isStreaming()) {
            Log.d(TAG, "stopping Stream");
            uploadBandwidthSampler.stopSampling();
            rtspCamera.stopStream();
            prepareInputs();
            stopPreview();
            startPreview();
        }
    }

    public void startRecord(String recordPath) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
        String currentDateAndTime = sdf.format(new Date());
        startRecord(recordPath, currentDateAndTime);
    }

    public void startRecord(String recordPath, String recordName) {
        if (rtspCamera != null && !rtspCamera.isRecording()) {
            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                    + recordPath);
            if (!folder.exists())
                folder.mkdir();

            try {
                rtspCamera.startRecord(folder.getAbsolutePath() + "/" + recordName + ".mp4");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopRecord() {
        if (rtspCamera != null && rtspCamera.isRecording())
            rtspCamera.stopRecord();
    }

    public void switchCamera() {
        rtspCamera.switchCamera();
    }

    public void disableVideo() {
        if (rtspCamera != null && rtspCamera.isVideoEnabled())
            rtspCamera.disableVideo();
    }

    public void enableVideo() {
        if (rtspCamera != null && !rtspCamera.isVideoEnabled())
            rtspCamera.enableVideo();
    }

    class Checker implements ConnectCheckerRtsp {
        @Override
        public void onConnectionSuccessRtsp() {
            Log.d(TAG, "onConnectionSuccessRtsp");
        }

        @Override
        public void onConnectionFailedRtsp(String s) {
            Log.d(TAG, "onConnectionFailedRtsp: " + s);
        }

        @Override
        public void onDisconnectRtsp() {
            Log.d(TAG, "onDisconnectRtsp");
        }

        @Override
        public void onAuthErrorRtsp() {
            Log.d(TAG, "onAuthErrorRtsp");
        }

        @Override
        public void onAuthSuccessRtsp() {
            Log.d(TAG, "onAuthSuccessRtsp");
        }
    }

    private void showLevelSupported() {
        try {
            showLevelSupported(((CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE)).getCameraCharacteristics("1"));
            showLevelSupported(((CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE)).getCameraCharacteristics("0"));
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void showLevelSupported(CameraCharacteristics c) {
        int deviceLevel = c.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        if (deviceLevel == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY) {
            Log.i(TAG, "Level supported: legacy");
        } else if (deviceLevel == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_3) {
            Log.i(TAG, "Level supported: level 3");
        } else if (deviceLevel == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_FULL) {
            Log.i(TAG, "Level supported: full");
        } else if (deviceLevel == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED) {
            Log.i(TAG, "Level supported: limited");
        }
    }
}