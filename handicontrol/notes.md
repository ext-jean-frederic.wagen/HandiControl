### Pair Watch to phone

1. Debug over wifi
2. Connect to phone shared wifi (no password)
3. Connect PC to same network
4. Search Watch IP address (on watch or on connected devices on phone)
5. `adb connect 192.168.right.ip:5555`
6. `adb devices`
7. Disable bluetooth on phone
8. `adb shell “pm clear com.google.android.gms && reboot”`
9. Install Wear OS app on phone
10. Reconnect watch to wifi like in 5.
11. `adb shell “am start -a android.bluetooth.adapter.action.REQUEST_DISCOVERABLE”`
12. Connect watch through Wear OS app
