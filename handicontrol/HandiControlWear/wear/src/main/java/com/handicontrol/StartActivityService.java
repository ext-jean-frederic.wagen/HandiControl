package com.handicontrol;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.charset.Charset;

public class StartActivityService extends WearableListenerService {
    private static final String TAG = "START_ACTIVITY_SERVICE";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        byte[] data = messageEvent.getData();
        String message = new String(data, Charset.forName("UTF-8"));
        String path = messageEvent.getPath();

        Log.d(TAG, "Received a message from app: " + path + " / " + message);

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
